#include <SPI.h>
#include <PN532_SPI.h>
#include <PN532Interface.h>
#include <PN532.h>
#define Cursor ':'
PN532_SPI pn532spi(SPI, 10);
PN532 nfc(pn532spi);

//Commands:
// 1 - get payment token
// 2 - send user room key

void setup()
{    
    Serial.begin(115200);
    Serial.println("-------PAYMENT SYSTEM STARTING NOW --------");
    
    nfc.begin();
    
    uint32_t versiondata = nfc.getFirmwareVersion();
    if (! versiondata) {
      Serial.print("Didn't find PN53x board");
      while (1); // halt
    }
    
    Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX); 
    Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC); 
    Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);
    
    nfc.SAMConfig();
}

void loop()
{
 
  uint8_t selectApdu[] = { 
          0x00, /* CLA */
          0xA4, /* INS */
          0x04, /* P1  */
          0x00, /* P2  */
          0x07, /* Length of AID  */
          0xF0, 0x39, 0x41, 0x48, 0x14, 0x81, 0x00,
           0x00  /* Le  */};

  uint8_t selectRoomApdu[] = { 
          0x00, /* CLA */
          0xA4, /* INS */
          0x04, /* P1  */
          0x00, /* P2  */
          0x07, /* Length of AID  */
          0xF0, 0x39, 0x41, 0x48, 0x14, 0x81, 0x01,
          0x00  /* Le  */};
           
  bool success;
  
  uint8_t responseLength = 255;
  String command;
  uint8_t data[responseLength];
  bool commandData = true;
  byte index = 0;
  
     if (Serial.available() > 0) //Patikrina ar nuokliame prievade
     {                            //nėra ateinančių duomenų
     
         char element = Serial.read(); //Perskaitomas vienas bitas
         if(element == Cursor)  //Patikriname ar tai ne atskiriamasis elementas
         {
            commandData = false;
         }    
         if(commandData == true)
         {
            command += element;  //Komandos tekstas
         }else
         {
            data[index] = element;  //Duomenų tekstas
            index++;
         }   
     } 
     
     if(command.length() > 0)
     {
              command.trim();
            
              Serial.println(command);
              //Serial.println(data);
              if(command == "1") //GET TOKEN COMMAND
              { 
                boolean working = true;
                
                while(working == true)
                {
                   
                  Serial.println("Waiting for an Android Phone");
           
                  success = nfc.inListPassiveTarget(); //Discover phone -> true
          
                  if(success) 
                  {
                        Serial.println("Found something");
                                                               
                        uint8_t response[255];
                        uint8_t responseLength = sizeof(response);
                     
                        success = nfc.inDataExchange(selectApdu, sizeof(selectApdu), response, &responseLength);
                    
                        if(success) 
                        {
  
                              String responseData = dataToString(response, responseLength);
                              if((responseData != "NEED_AUTHORIZATION") && (responseData != "NO_PAYMENT_CARD"))
                              {
                                  Serial.println("exePayment"); 
                                  Serial.println(responseData);
                                  working = false; 
                              }else if(responseData == "NO_PAYMENT_CARD"){
                                  working = false; 
                              }
                        }
                        else 
                        {  
                              Serial.println("Failed sending SELECT AID"); 
                        }
                 }
                 else 
                 {
                        Serial.println("Didn't find anything");
                 }
                    
                 delay(1000);                 
                }
                
              }
              else if(command == "2") //SEND ROOM KEY (AFTER PAYMENT)
              {
                boolean working = true;
                
                while(working == true)
                {
                   
                  Serial.println("Waiting for an Android Phone to send room key");
           
                  success = nfc.inListPassiveTarget(); //Discover phone -> true
          
                  if(success) 
                  {
                        Serial.println("Found something");
                                                               
                        uint8_t response[255];  
                     
                        success = nfc.inDataExchange(selectRoomApdu, sizeof(selectRoomApdu), response, &responseLength);

                        if(success) 
                        {
                          //do {
                              
                              //uint8_t response[255]; 
                             // success = nfc.inDataExchange(data, sizeof(data), response, &responseLength);
                              String responseData = dataToString(response, responseLength);
                              if(responseData == "OK")
                              {
                                  Serial.println("Room key sent"); 
                                  working = false; 
                              }

                          //}
                          //while(working == true);


                              
                        }
                        else 
                        {  
                              Serial.println("Failed sending SELECT AID"); 
                        }
                 }
                 else 
                 {
                        Serial.println("Didn't find anything");
                 }
                    
                 delay(1000);
                
              }
              }
              else
              {
                Serial.println("No command");
              }
          }
    }


String dataToString(uint8_t *response, uint8_t responseLength) {
  
   String respBuffer;

    for (int i = 0; i < responseLength; i++) {
      
    
      respBuffer = respBuffer + (char)response[i];                        
    }

    return respBuffer;
}
