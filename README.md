# NFC-payment-system

NFC payment prototype system for hotel insfrastructure.
Payment execution used Arduino NFC controller to send data between Android phone and hotel system.

This prototype system is for to pay for hotel.

This project includes:

    +ASP.NET WEB API prototype application with ASP.NET Identity 2.1 (token based authentication)
    +Android application. This app is using HCE ( Host Card Emulation) method to send and receive (bidirectional communication) data from NFC controller system.
    +Hotel worker app using Winforms.
    +In this prototype used Arduino UNO R3 and PN532 NFC modules.
      
Features:

    +Android app:
      *Create a user account.
      *Data exchange with WEB services.
      *Add/Remove payment cards in user account.
        -When you add a payment card using phone, mobile app sends a payment card information to WEB api (before sending to service the phone encrypts a payment card information).
        -In service generates a special token (uses AES cryptography algorithm to secure information) and it sends back to user phone and saves that information.
      *Pay for hotel room using mobile with or without internet connection.

    +Hotel worker app:
      *Reserve a hotel room.
        -Creates a hotel reservation using database information.
      *Control a Arduino controller (get all controller log actions). 
        -Gets information from Arduino controller what was sent from created android app.
        -Sends data (reservation, token data) to Arduino controller.
      *Shows logs. 

    +WEB services (prototype bank, hotel and own application services):
        Creates simple communication between them using REST API.


