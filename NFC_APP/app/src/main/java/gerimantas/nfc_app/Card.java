package gerimantas.nfc_app;

import java.util.Date;

/**
 * Created by Gerimantas on 2016-05-01.
 */
public class Card {
    private String cardNumber;
    private String token;
    private String bank;
    private Date expired;
    private String userId;
    private int timesToUse;
    private int id;

    public Card (String token, String bank, Date expired, int id, String userId, String cardNumber, int timesToUse){
        this.token = token;
        this.bank = bank;
        this.expired = expired;
        this.id = id;
        this.userId = userId;
        this.cardNumber = cardNumber;
        this.timesToUse = timesToUse;
    }

    public void setTimesToUse(int timesToUse){
        this.timesToUse = timesToUse;
    }
    public void setToken(String token){
        this.token = token;
    }

    public int getTimesToUse() {return timesToUse;}

    public String getCardNumber() {return cardNumber;}

    public String getToken() {return token;}

    public String getBank() {
        return bank;
    }

    public int getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public Date getExpired() {
        return expired;
    }
}
