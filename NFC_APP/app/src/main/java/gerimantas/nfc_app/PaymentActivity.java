package gerimantas.nfc_app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

public class PaymentActivity extends AppCompatActivity {

    private final String TAG = "PAYMENT_ACTIVITY";
    private AlertDialog.Builder dialogBuilder;
    private View dialogView;
    private android.support.v7.app.AlertDialog alertDialog;
    private EditText etPassword;
    private final String tempPassword = "123456";   //TESTING -> AUTHENTICATION PASSWORD
    private  BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        //String value = getIntent().getStringExtra("hcedata");
        //Log.d("value", value);
        final Button cancelPayment = (Button) findViewById(R.id.buttonCancelPayment);
        final TextView textInfoPayment = (TextView) findViewById(R.id.infoPayment);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        cancelPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAuthentication(true);
                finish();
            }
        });

        AuthenticationDialogBox();


        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String s = intent.getStringExtra("DATA");
                  if(s.equals("ERROR")){
                      progressBar.setVisibility(View.GONE);
                      cancelPayment.setText("Atgal");
                      textInfoPayment.setText("Mokėjimas neįvykdytas, žetonų kiekis pasibaigė");
                  }else if(s.equals("ACCPEPT")){
                      cancelPayment.setText("Atgal");
                      progressBar.setVisibility(View.GONE);
                      textInfoPayment.setText("Mokėjimas įvykdytas");
                  }
            }
        };

        Log.d(TAG, "PAYMENT ACTIVATED SENDING TOKEN TO POS ");
    }
    private void setAuthentication(boolean b){

        SharedPreferences preferences = getApplicationContext().getSharedPreferences("user_data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("authorization", b);
        editor.commit();
    }


    private void AuthenticationDialogBox() {
            dialogBuilder = new AlertDialog.Builder(this);
            dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_box_authentication_payment, null);
            dialogBuilder.setView(dialogView);

            dialogBuilder.setPositiveButton("Patvirtinti", null);
            dialogBuilder.setNegativeButton("Atmesti", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //moveTaskToBack(true);
                    setAuthentication(true);
                    finish();
                }
            });

            alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener( new View.OnClickListener(){

                @Override
                public void onClick(View v) {

                    etPassword = (EditText) dialogView.findViewById(R.id.authentication_payment_dialog_password_input);
                    String userPassword = etPassword.getText().toString();
                    if (userPassword.equals(tempPassword)) {
                        setAuthentication(false);
                        alertDialog.dismiss();
                    }
                }
            });
    }


    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter("PAYMENT")
        );
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onStop();
    }

}
