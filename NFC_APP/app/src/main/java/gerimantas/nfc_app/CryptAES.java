package gerimantas.nfc_app;

import android.util.Base64;
import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Gerimantas on 2016-05-10.
 */
public class CryptAES {


    private final String characterEncoding = "UTF-8";
    private final String cipherTransformation = "AES"; //NoPadding //PKCS5Padding ///CBC/NoPadding
    private final String aesEncryptionAlgorithm = "AES";

    public  byte[] decrypt(byte[] cipherText, byte[] key, byte [] initialVector) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException
    {
        Cipher cipher = Cipher.getInstance(cipherTransformation);
        SecretKeySpec secretKeySpecy = new SecretKeySpec(key, aesEncryptionAlgorithm);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpecy, ivParameterSpec);
        cipherText = cipher.doFinal(cipherText);
        return cipherText;
    }

    public byte[] encrypt(byte[] plainText, byte[] key, byte [] initialVector) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException
    {
        Cipher cipher = Cipher.getInstance(cipherTransformation);
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, aesEncryptionAlgorithm);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);

        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
        plainText = cipher.doFinal(plainText);
        return plainText;
    }

//    private byte[] getKeyBytes(String key, int keyByteLenght) throws UnsupportedEncodingException {
//        byte[] keyBytes= new byte[keyByteLenght];
//        byte[] parameterKeyBytes= key.getBytes(characterEncoding);
//        System.arraycopy(parameterKeyBytes, 0, keyBytes, 0, Math.min(parameterKeyBytes.length, keyBytes.length));
//        return keyBytes;
//    }

    public String encrypt(String value, String key, String initVector) throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException{
        int keyByteLenght = key.length()/8;
        //byte[] keyBytes = getKeyBytes(key, keyByteLenght);
        byte[] valueBytes = value.getBytes(characterEncoding);
        byte[] keyBytes = key.getBytes(characterEncoding);
        byte[] initVectorBytes = initVector.getBytes(characterEncoding);

        return Base64.encodeToString(encrypt(valueBytes,keyBytes, initVectorBytes), Base64.DEFAULT);
    }

    public String decrypt(String encryptedText, String key, String initVector) throws KeyException, GeneralSecurityException, GeneralSecurityException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException {
        int keyByteLenght = key.length()/8;
        //byte[] keyBytes = getKeyBytes(key, keyByteLenght);
        byte[] cipheredBytes = Base64.decode(encryptedText, Base64.DEFAULT);
        byte[] keyBytes = key.getBytes(characterEncoding);
        byte[] initVectorBytes = initVector.getBytes(characterEncoding);

        return new String(decrypt(cipheredBytes, keyBytes, initVectorBytes), characterEncoding);
    }
}
