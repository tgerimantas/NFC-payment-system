package gerimantas.nfc_app;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.cardemulation.HostApduService;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Gerimantas on 2016-04-22.
 */

public class HostApduService0 extends android.nfc.cardemulation.HostApduService {

    private final String TAG = "HCE"; //F0394148148100
    private static final byte[] APDU_SELECT_PAYMENT = {
            (byte)0x00, // CLA  - Class - Class of instruction
            (byte)0xA4, // INS  - Instruction - Instruction code
            (byte)0x04, // P1   - Parameter 1 - Instruction parameter 1
            (byte)0x00, // P2   - Parameter 2 - Instruction parameter 2
            (byte)0x07, // Lc field - Number of bytes present in the data field of the command
            (byte)0xF0, (byte)0x39, (byte)0x41, (byte)0x48, (byte)0x14, (byte)0x81, (byte)0x00, // NDEF Tag Application name  //F0394148148100
            (byte)0x00  // Le field - Maximum number of bytes expected in the data field of the response to the command
    };

    private static final byte[] OK_SW = {(byte) 0x90, (byte) 0x00};
    private static final byte[] UNKNOWN_CMD_SW = {(byte) 0x00, (byte) 0x00};

    private static final byte[] APDU_SELECT_HOTEL_KEY = {
            (byte)0x00, // CLA  - Class - Class of instruction
            (byte)0xA4, // INS  - Instruction - Instruction code
            (byte)0x04, // P1   - Parameter 1 - Instruction parameter 1
            (byte)0x00, // P2   - Parameter 2 - Instruction parameter 2
            (byte)0x07, // Lc field - Number of bytes present in the data field of the command
            (byte)0xF0, (byte)0x39, (byte)0x41, (byte)0x48, (byte)0x14, (byte)0x81, (byte)0x01, // NDEF Tag Application name  //F0394148148100
            (byte)0x00  // Le field - Maximum number of bytes expected in the data field of the response to the command
    };

    @Override
    public byte[] processCommandApdu(byte[] commandApdu, Bundle extras) {
        LocalBroadcastManager broadcaster = LocalBroadcastManager.getInstance(this);
        Log.d("HCE", "Received APDU: " + ByteArrayToHexString(commandApdu));
        byte[] data;
        if (Arrays.equals(APDU_SELECT_PAYMENT, commandApdu)) {
            boolean check = false;
            SharedPreferences preferences = getApplicationContext().getSharedPreferences("user_data", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            String token = preferences.getString("selected_token", "");
            String cardID = preferences.getString("selected_token_id", "");
            if(!token.equals("")) {
                if (!isForeground("PaymentActivity") == true) {
                    Intent intent = new Intent(this, PaymentActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    //intent.putExtra("hcedata", new String(commandApdu));
                    startActivity(intent); //Atidaromas langas, kuriame įrašomas slaptažodis
                }
                if (getAuthorization()) {
                    data = "NEED_AUTHORIZATION".getBytes(); //Return data
                    Log.d("HCE", "NEED_AUTHORIZATION");
                    sendResponseApdu(data);
                } else {
                    //SharedPreferences preferences = getApplicationContext().getSharedPreferences("user_data", Context.MODE_PRIVATE);
                    //SharedPreferences.Editor editor = preferences.edit();
                    //editor.putBoolean("authorization", true);
                    //editor.commit();


                    String cardsJson = preferences.getString("cards", "");
                    Gson gson = new Gson();
                    ArrayList<Card> objects;
                    Type type = new TypeToken<ArrayList<Card>>() {}.getType();
                    objects = (ArrayList<Card>) gson.fromJson(cardsJson, type);
                    Log.d(TAG, String.valueOf(objects.size()));

                    ArrayList<Card> objectsNew = new ArrayList<Card>();
                    for (int i = 0; i < objects.size(); i++) {
                        if (String.valueOf(objects.get(i).getId()).equals(cardID)) {
                            if(objects.get(i).getTimesToUse() <= 0) {
                                check = true;
                            }else{
                                int times = objects.get(i).getTimesToUse() - 1;
                                objects.get(i).setTimesToUse(times);

                            }
                            objectsNew.add(objects.get(i));
                        }else{
                            objectsNew.add(objects.get(i));
                        }
                    }
                    String json = gson.toJson(objectsNew);
                    editor.putString("cards", json);
                    editor.commit();

                    if(check == true){
                        //
                        String message ="ERROR";
                        Intent intent = new Intent("PAYMENT");
                        if(message != null)
                            intent.putExtra("DATA", message);
                        broadcaster.sendBroadcast(intent);
                        return "NO_PAYMENT_CARD".getBytes();
                    }else{
                        //
                        String message ="ACCPEPT";
                        Intent intent = new Intent("PAYMENT");
                        if(message != null)
                            intent.putExtra("DATA", message);
                        broadcaster.sendBroadcast(intent);
                        Log.d("HCE", "Send data");
                        data = getPaymentData().getBytes(); //Return data
                        sendResponseApdu(data);
                    }

                }

                return null;
            }else{
                Intent intent = new Intent(this, ErrorPaymentActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                return "NO_PAYMENT_CARD".getBytes();
            }
        }
        else if(Arrays.equals(APDU_SELECT_HOTEL_KEY, commandApdu)){
            SharedPreferences preferences = getApplicationContext().getSharedPreferences("user_data", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("roomToken", "123456789");
            return "OK".getBytes();
        }
        else{
            return UNKNOWN_CMD_SW;
        }

    }

    public boolean isForeground(String myPackage) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
        Log.d("HCE",componentInfo.getClassName() );
        return ("gerimantas.nfc_app." + myPackage).equals(componentInfo.getClassName().toString());
    }

    private String getPaymentData(){

        SharedPreferences preferences = getApplicationContext().getSharedPreferences("user_data", Context.MODE_PRIVATE);
        String token = preferences.getString("selected_token", "");
        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        String cardsJson = preferences.getString("user", "");
        Gson gson = new Gson();
        User user;
        Type type = new TypeToken<User>(){}.getType();
        user = (User)gson.fromJson(cardsJson, type);

        return user.getId()+":"+token+":"+android_id; //UserID; Token; AndroidID;
    }

    private boolean getAuthorization() {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("user_data", Context.MODE_PRIVATE);
        Boolean authorization = preferences.getBoolean("authorization", true);
        return authorization;
    }
    public void onDeactivated(int reason) {

    }

    public static String ByteArrayToHexString(byte[] bytes) {
        final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}