package gerimantas.nfc_app;

import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by Gerimantas on 2016-05-12.
 */
public class Crypt {


    public String encryptionRSA(String key, PublicKey publicKey){
        long encryptInitialTime = 0;
        long encryptEndTime = 0;
        String encryptedKey = null;

        CryptRSA rsa = new CryptRSA();
        try {
            encryptInitialTime = System.nanoTime();
            encryptedKey = rsa.encrypt(key, publicKey);
            encryptEndTime = System.nanoTime();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }


        double encryptDifference = (encryptEndTime - encryptInitialTime)/1e6;
        Log.d("RSA ENCRYPT 2048", encryptedKey);
        Log.d("RSA ENCRYPT 2048", String.valueOf(encryptDifference));

        return encryptedKey;
    }

    public String decryptionRSA(String encryptedKey, PrivateKey privateKey){
        long decryptInitialTime = 0;
        long decryptEndTime = 0;
        String decryptedKey = null;

        CryptRSA rsa = new CryptRSA();

        try {
            decryptInitialTime = System.nanoTime();
            decryptedKey = rsa.decrypt(encryptedKey,privateKey);
            decryptEndTime = System.nanoTime();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        double decryptDifference = (decryptEndTime - decryptInitialTime)/1e6;
        Log.d("RSA DECRYPT 2048", encryptedKey);
        Log.d("RSA DECRYPT 2048", decryptedKey);
        Log.d("RSA DECRYPT 2048", String.valueOf(decryptDifference));
        return decryptedKey;
    }

    public String encryptionAES(String cardNumber, String key, String initVector){
        long encryptInitialTime = 0;
        long encryptEndTime = 0;
        String encryptedData = null;

        if(key.getBytes().length == 16 || key.getBytes().length == 32) {

            CryptAES aes = new CryptAES();
            try {
                encryptInitialTime = System.nanoTime();
                encryptedData = aes.encrypt(cardNumber, key, initVector);
                encryptEndTime = System.nanoTime();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (InvalidAlgorithmParameterException e) {
                e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            }

            double encryptDifference = (encryptEndTime - encryptInitialTime)/1e6;
            Log.d("AES ENCRYPT 128", cardNumber);
            Log.d("AES ENCRYPT 128", encryptedData);
            Log.d("AES ENCRYPT 128", String.valueOf(encryptDifference));

            return encryptedData;
        }else{
            return null;
        }
    }

    public String decryptionAES( String encryptedData, String key, String initVector ){

        long decryptInitialTime = 0;
        long decryptEndTime = 0;
        String decryptedData = null;

       // if(key.getBytes().length == 16 || key.getBytes().length == 32) {

            CryptAES aes = new CryptAES();
            try {
                decryptInitialTime = System.nanoTime();
                decryptedData = aes.decrypt(encryptedData, key, initVector);
                decryptEndTime = System.nanoTime();
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            double decryptDifference = (decryptEndTime - decryptInitialTime)/1e6;
            Log.d("AES DECRYPT 128", encryptedData);
            Log.d("AES DECRYPT 128", decryptedData);
            Log.d("AES DECRYPT 128", String.valueOf(decryptDifference));
            return decryptedData;
//        }else{
//            return null;
//        }
    }


    public  EncryptionData encryptionAES(PublicKey publicKey, PrivateKey privateKey, String cardNumber){
        //TESTING
        //String key = "Bar12345Bar12345"; // 128bite
        //String key = "Bar12345Bar12345Bar12345Bar12345"; // 256bit
        String initVector = "1234123412341234"; //16bit
        //String data =  "101" + ":" + "2016-01-31" + ":" + "2016-02-05";
        String data = "1234" +
                "";
        String key = "0123456789010123"; // 128bite
        String encryptedData = encryptionAES(data, key, initVector);
        String encryptedKey = encryptionRSA(key, publicKey);
        String decryptedKey = decryptionRSA(encryptedKey, privateKey);
        byte[] asa = key.getBytes();
        Log.d("777", asa.toString());
        //Log.d("777", String.valueOf(decryptedKey.getBytes().length));
        Log.d("777", String.valueOf(encryptedData.getBytes().length));
        String decryptedData = decryptionAES(encryptedData, key, initVector);

        return new EncryptionData(encryptedData, null); //encryptedKey
    }


    public  EncryptionData encryptionDES(PublicKey publicKey, PrivateKey privateKey, String cardNumber){
//        //TESTING
//        String key = "Bar12345Bar12345"; // 128bite
//        //String key = "Bar12345Bar12345Bar12345Bar12345"; // 256bit
//        String initVector = "RandomInitVector"; //16bit
//        //String data =  "101" + ":" + "2016-01-31" + ":" + "2016-02-05";
//        String data = "0001001:01-31:02-31";

        //String encryptedData = encryptionDES(cardNumber, key, initVector);
        //String encryptedKey = encryptionRSA(key, publicKey);
        //String decryptedKey = decryptionRSA(encryptedKey, privateKey);
        //String decryptedData = decryptionAES(encryptedData, decryptedKey, initVector);

        return new EncryptionData(null, null);
    }

    public String encryptionDES(String cardNumber){
        long encryptInitialTime = 0;
        long encryptEndTime = 0;
        String encryptedData = null;
        String key = "Bar12345Bar12345"; // 128bit
        //String key = "Bar12345Bar12345Bar12345Bar12345"; // 256bit
        String initVector = "RandomInitVector"; //16bit
        if(key.getBytes().length == 16 || key.getBytes().length == 32) {

            Crypt3DES des = new Crypt3DES();
            try {
                encryptInitialTime = System.nanoTime();
                encryptedData = des.encrypt(cardNumber, key, initVector);
                encryptEndTime = System.nanoTime();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (InvalidAlgorithmParameterException e) {
                e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            }

            double encryptDifference = (encryptEndTime - encryptInitialTime)/1e6;
            Log.d("AES ENCRYPT 128", cardNumber);
            Log.d("AES ENCRYPT 128", encryptedData);
            Log.d("AES ENCRYPT 128", String.valueOf(encryptDifference));

            return encryptedData;
        }else{
            return null;
        }
    }

    public String decryptionDES( String encryptedData ){

        long decryptInitialTime = 0;
        long decryptEndTime = 0;
        String decryptedData = null;
        String key = "Bar12345Bar12345"; // 128bit
        //String key = "Bar12345Bar12345Bar12345Bar12345"; // 256bit
        String initVector = "RandomInitVector"; //16bit
        if(key.getBytes().length == 16 || key.getBytes().length == 32) {

            Crypt3DES des = new Crypt3DES();
            try {
                decryptInitialTime = System.nanoTime();
                decryptedData = des.decrypt(encryptedData, key, initVector);
                decryptEndTime = System.nanoTime();
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            double decryptDifference = (decryptEndTime - decryptInitialTime)/1e6;
            Log.d("AES DECRYPT 128", encryptedData);
            Log.d("AES DECRYPT 128", decryptedData);
            Log.d("AES DECRYPT 128", String.valueOf(decryptDifference));
            return decryptedData;
        }else{
            return null;
        }
    }

}
