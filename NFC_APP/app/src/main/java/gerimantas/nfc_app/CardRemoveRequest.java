package gerimantas.nfc_app;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Gerimantas on 2016-05-08.
 */
public class CardRemoveRequest extends StringRequest {

    private static String REMOVE_CARD_REQUEST_URL = "http://test:49711/api/account/removecard";
    private static String TAG = "REMOVE CARD";
    private Map<String, String> parameters;
    private Context context;

    public CardRemoveRequest(Context context, int id, Response.Listener<String> listener, Response.ErrorListener errorListener) {

            super(Request.Method.POST, REMOVE_CARD_REQUEST_URL, listener, errorListener);
            this.context = context;
            Log.d(TAG, "Initialize request");
            Log.d(TAG, "-> "+ REMOVE_CARD_REQUEST_URL);
            Log.d(TAG, "-> "+ id);
            parameters = new HashMap<String, String>();
            parameters.put("tokenId", String.valueOf(id));
            //parameters.put("encryptedKey", encryptedKey);
        }
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            SharedPreferences preferences = context.getSharedPreferences("user_data", Context.MODE_PRIVATE);
            Map<String, String> headers = new HashMap<String, String>();
            headers.put("Accept","application/json");
            headers.put("Authorization", "Bearer " + preferences.getString("token", null));
            return headers;
        }
        @Override
        public Map<String, String> getParams() {
            return parameters;
        }

    }
