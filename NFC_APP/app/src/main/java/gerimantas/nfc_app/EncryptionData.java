package gerimantas.nfc_app;

/**
 * Created by Gerimantas on 2016-05-14.
 */
public class EncryptionData{
    private String encryptedData;
    private String encryptedKey;

    public EncryptionData(String encryptedData, String encryptedKey){
        this.encryptedData = encryptedData;
        this.encryptedKey = encryptedKey;
    }

    public String getEncryptedData() {
        return encryptedData;
    }

    public String getEncryptedKey() {
        return encryptedKey;
    }

}