package gerimantas.nfc_app;

/**
 * Created by Gerimantas on 2016-05-01.
 */
public class CreditCardView {
    private String creditCardNumber;
    private String creditCardType;

    public CreditCardView(String creditCardNumber, String creditCardType){
        this.creditCardNumber = creditCardNumber;
        this.creditCardType = creditCardType;
    }

    public String getCreditCardNumber(){
        return creditCardNumber;
    }

    public String getCreditCardType(){
        return creditCardType;
    }


}
