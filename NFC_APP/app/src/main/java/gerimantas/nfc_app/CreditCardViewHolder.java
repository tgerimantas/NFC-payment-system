package gerimantas.nfc_app;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Gerimantas on 2016-05-01.
 */
public class CreditCardViewHolder extends RecyclerView.ViewHolder {
    protected TextView creditCardType;
    protected TextView creditCardNumber;
    protected TextView paymentCardTimes;

    public CreditCardViewHolder(View itemView) {
        super(itemView);
        creditCardType = (TextView) itemView.findViewById(R.id.credit_card_type);
        creditCardNumber = (TextView) itemView.findViewById(R.id.credit_card_number);
        paymentCardTimes = (TextView) itemView.findViewById(R.id.payment_card_times);
    }
}
