package gerimantas.nfc_app;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

/**
 * Created by Gerimantas on 2016-04-15.
 */
public class UserFragment extends android.support.v4.app.Fragment {

    private  String TAG = "USER";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences preferences = getActivity().getSharedPreferences("user_data", Context.MODE_PRIVATE);
        boolean isOffile = preferences.getBoolean("isOffile", false);

        if(isOffile != true) {
            Response.Listener<String> listener = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject object = new JSONObject(response);

                        if (object.get("id").toString() != null) {

                            SharedPreferences preferences = getActivity().getSharedPreferences("user_data", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            String cardsJson = preferences.getString("user", "");

                            //Save user info
                            Gson gson = new Gson();
                            User user;
                            Type type = new TypeToken<User>() {}.getType();
                            user = (User) gson.fromJson(cardsJson, type);
                            user = new User(object.get("firstName").toString(), object.get("lastName").toString(), object.get("email").toString(), object.get("id").toString());

                            String json = gson.toJson(user);
                            editor.putString("user", json);
                            editor.commit();

                        } else {
                            Toast.makeText(getActivity().getApplicationContext(), "Error: ", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        Toast.makeText(getActivity().getApplicationContext(), "Error: ", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            };

            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "" + error);
                    Toast.makeText(getActivity().getApplicationContext(), "Ryšio klaida" + error, Toast.LENGTH_LONG).show();
                }
            };

            UserRequest userRequest = new UserRequest(getContext(), listener, errorListener);
            RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
            userRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(userRequest);
        }

        LayoutInflater mInflater = (LayoutInflater) getContext()
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View convertView = mInflater.inflate(R.layout.fragment_user, null);

        //final TextView firstName = (TextView) convertView.findViewById(R.id.userFragmentFirstName);
        //final TextView lastName = (TextView) convertView.findViewById(R.id.userFragmentLastName);

        String userData = preferences.getString("user", "");
        Gson gson = new Gson();
        User user;
        Type type = new TypeToken<User>() {}.getType();
        user = (User) gson.fromJson(userData, type);

        //firstName.setText("Geris");
        //lastName.setText("T");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
       return inflater.inflate(R.layout.fragment_user, parent, false);

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

    }
}
