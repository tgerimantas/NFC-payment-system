package gerimantas.nfc_app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class PaymentCardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_card);


        SharedPreferences preferences = this.getSharedPreferences("user_data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        String selected_token_id = preferences.getString("selected_token_id", "");

        Card card = getCard(selected_token_id);

        final Button paymentButton = (Button) findViewById(R.id.paymentCardPayButton);
        final TextView paymentCardBank = (TextView) findViewById(R.id.paymentCardBank);
        final TextView paymentCardToken = (TextView) findViewById(R.id.paymentCardToken);
        final TextView paymentCardValid = (TextView) findViewById(R.id.paymentCardValid);
        //final TextView paymentCardToken = (TextView) findViewById(R.id.paymentCardToken);
        paymentCardBank.setText(card.getBank());
        paymentCardToken.setText(card.getToken());
        paymentCardValid.setText(card.getExpired().toString());


        paymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentCardActivity.this, PaymentActivity.class);
                PaymentCardActivity.this.startActivity(intent);
            }


        });
    }

    private Card getCard(String selected_token_id) {

        SharedPreferences preferences = this.getSharedPreferences("user_data", Context.MODE_PRIVATE);
        String cardsJson = preferences.getString("cards", "");
        Gson gson = new Gson();
        Type typeUser = new TypeToken<User>(){}.getType();


        Card selectedCard =null;

        Type type = new TypeToken<ArrayList<Card>>(){}.getType();
        ArrayList<Card> objects = (ArrayList<Card>) gson.fromJson(cardsJson, type);
        if(objects != null) {
            for (int i = 0; i < objects.size(); i++) {
                if (String.valueOf(objects.get(i).getId()).equals(selected_token_id)) {
                    selectedCard = objects.get(i);
                }
            }
        }

        return selectedCard;
    }
}
