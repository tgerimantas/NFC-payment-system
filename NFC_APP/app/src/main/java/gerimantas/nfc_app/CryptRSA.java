package gerimantas.nfc_app;

import android.util.Base64;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Gerimantas on 2016-05-11.
 */
public class CryptRSA {

    private final String characterEncoding = "UTF-8";
    private final String cipherTransformation = "RSA";
    private final String aesEncryptionAlgorithm = "RSA";

    public  byte[] decrypt(byte[] cipherText, PrivateKey key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException
    {
        Cipher cipher = Cipher.getInstance(cipherTransformation);
        cipher.init(Cipher.DECRYPT_MODE, key);
        cipherText = cipher.doFinal(cipherText);
        return cipherText;
    }

    public byte[] encrypt(byte[] plainText, PublicKey key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException
    {
        Cipher cipher = Cipher.getInstance(cipherTransformation);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        plainText = cipher.doFinal(plainText);
        return plainText;
    }

//    private byte[] getKeyBytes(String key, int keyByteLenght) throws UnsupportedEncodingException {
//        byte[] keyBytes= new byte[keyByteLenght];
//        byte[] parameterKeyBytes= key.getBytes(characterEncoding);
//        System.arraycopy(parameterKeyBytes, 0, keyBytes, 0, Math.min(parameterKeyBytes.length, keyBytes.length));
//        return keyBytes;
//    }

    public String encrypt(String value, PublicKey key) throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException{

        //byte[] keyBytes = getKeyBytes(key, keyByteLenght);
        byte[] valueBytes = value.getBytes(characterEncoding);
        return Base64.encodeToString(encrypt(valueBytes,key), Base64.DEFAULT);
    }

    public String decrypt(String encryptedText, PrivateKey key) throws KeyException, GeneralSecurityException, GeneralSecurityException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException {
        //byte[] keyBytes = getKeyBytes(key, keyByteLenght);
        byte[] cipheredBytes = Base64.decode(encryptedText, Base64.DEFAULT);
        return new String(decrypt(cipheredBytes, key), characterEncoding);
    }
}
