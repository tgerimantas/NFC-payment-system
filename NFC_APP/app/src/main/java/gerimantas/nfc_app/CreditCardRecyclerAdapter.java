package gerimantas.nfc_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Gerimantas on 2016-05-01.
 */
public class CreditCardRecyclerAdapter extends RecyclerView.Adapter<CreditCardViewHolder> {

    private List<Card> creditCardViews;
    private int selected_position;
    private static Context context;
    private AlertDialog.Builder dialogBuilderDeleteBox;
    private View dialogDeleteBoxView;
    private String TAG = "CreditCardAdapter";
    private AlertDialog alertDialog;

    public CreditCardRecyclerAdapter(List<Card> creditCardViews, Context context){
         this.creditCardViews = new ArrayList<Card>();
         this.creditCardViews.addAll(creditCardViews);
         this.context = context;
    }

    @Override
    public CreditCardViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        final View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.credit_card_layout_view, parent, false);

        return new CreditCardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CreditCardViewHolder holder, final int position) {
         Card selectedCard = creditCardViews.get(position);
         holder.creditCardNumber.setText(selectedCard.getCardNumber());
         holder.creditCardType.setText(selectedCard.getBank());
         holder.paymentCardTimes.setText(String.valueOf(selectedCard.getTimesToUse()));

        if(selected_position == position){

            holder.itemView.setBackgroundColor(Color.GREEN);
        }else{
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Card selectedCard = creditCardViews.get(position);
                SharedPreferences preferences = context.getSharedPreferences("user_data", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("selected_token", selectedCard.getToken().toString());
                editor.putString("selected_token_id", String.valueOf(selectedCard.getId()));
                Log.d("SELECTED_TOKEN", selectedCard.getToken().toString());
                editor.commit();

                Intent intent = new Intent(context, PaymentCardActivity.class);
                context.startActivity(intent);

                notifyItemChanged(selected_position);
                selected_position = position;
                notifyItemChanged(selected_position);
            }
        });
        SharedPreferences preferences = context.getSharedPreferences("user_data", Context.MODE_PRIVATE);
        boolean isOffile = preferences.getBoolean("isOffile", true);
        if(isOffile == false) {
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View view) {

                    dialogBuilderDeleteBox = new AlertDialog.Builder(view.getContext());
                    dialogDeleteBoxView = LayoutInflater.from(view.getContext()).inflate(R.layout.delete_payment_card_dialog_box, null);
                    dialogBuilderDeleteBox.setView(dialogDeleteBoxView);

                    dialogBuilderDeleteBox.setPositiveButton("Pašalinti", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            final ProgressDialog progressDialog = new ProgressDialog(context, 0);
                            progressDialog.setIndeterminate(true);
                            progressDialog.setMessage("Pašalinama...");
                            progressDialog.show();

                            final Card selectedCard = creditCardViews.get(position);

                            Response.Listener<String> listener = new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject object = new JSONObject(response);
                                        Log.d(TAG, response);
                                        if (object.isNull("error")) {
                                            SharedPreferences preferences = context.getSharedPreferences("user_data", Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editor = preferences.edit();
                                            String cardsJson = preferences.getString("cards", "");

                                            Gson gson = new Gson();
                                            ArrayList<Card> objects;

                                            Type type = new TypeToken<ArrayList<Card>>() {
                                            }.getType();
                                            objects = (ArrayList<Card>) gson.fromJson(cardsJson, type);
                                            Log.d(TAG, String.valueOf(objects.size()));

                                            ArrayList<Card> objectsNew = new ArrayList<Card>();
                                            for (int i = 0; i < objects.size(); i++) {
                                                if (i != position) {
                                                    objectsNew.add(objects.get(i));
                                                }
                                            }

                                            objects.remove(selectedCard); //REMOVE CARD
                                            //Log.d(TAG, String.valueOf(objectsNew.size()));
                                            String selectedToken = preferences.getString("selected_token", "");
                                            if(selectedCard.getToken().equals(selectedToken)){
                                                preferences.edit().remove("selected_token_id");
                                                preferences.edit().remove("selected_token").commit();
                                            }

                                            if (objectsNew == null) {
                                                preferences.edit().remove("cards");
                                                preferences.edit().remove("selected_token_id");
                                                preferences.edit().remove("selected_token").commit();
                                            } else {
                                                String json = gson.toJson(objectsNew);
                                                editor.putString("cards", json);
                                                editor.commit();
                                            }
                                            creditCardViews.remove(selectedCard);
                                            notifyDataSetChanged();

                                            progressDialog.dismiss();

                                        } else {
                                            progressDialog.dismiss();
                                            Toast.makeText(context.getApplicationContext(), "Error: " + object.get("error").toString(), Toast.LENGTH_LONG).show();
                                        }
                                    } catch (JSONException e) {
                                        progressDialog.dismiss();
                                        e.printStackTrace();
                                    }
                                }
                            };

                            Response.ErrorListener errorListener = new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d(TAG, "" + error);
                                    progressDialog.dismiss();
                                    Toast.makeText(context.getApplicationContext(), "Ryšio klaida" + error, Toast.LENGTH_LONG).show();
                                }
                            };

                            CardRemoveRequest cardRemoveRequest = new CardRemoveRequest(context, selectedCard.getId(), listener, errorListener);
                            RequestQueue queue = Volley.newRequestQueue(context.getApplicationContext());
                            cardRemoveRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            queue.add(cardRemoveRequest);
                        }
                    });
                    dialogBuilderDeleteBox.setNegativeButton("Atmesti", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.cancel();
                        }
                    });
                    alertDialog = dialogBuilderDeleteBox.create();
                    alertDialog.show();
                    return false;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return creditCardViews.size();
    }


}
