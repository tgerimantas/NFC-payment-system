package gerimantas.nfc_app;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Gerimantas on 2016-04-08.
 * Makes request and gets data from server
 */
public class RegisterRequest extends StringRequest {

    private static String REGISTER_REQUEST_URL = "http://test:49711/api/account/create";
    private static String TAG = "REGISTRATION";
    private Map<String, String> parameters;

    public RegisterRequest(String firstName, String lastName, String userName, String email, String password, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, REGISTER_REQUEST_URL, listener, errorListener);
        parameters = new HashMap<String, String>();
        parameters.put("email", email);
        parameters.put("username", userName);
        parameters.put("firstName", firstName);
        parameters.put("lastName", lastName);
        parameters.put("roleName", "User");
        parameters.put("password", password);
        parameters.put("confirmPassword", password);
        parameters.put("grant-type", "password" );
        Log.d(TAG, "Initialize request");
        Log.d(TAG, "-> "+ REGISTER_REQUEST_URL);
    }
    @Override
    public Map<String, String> getHeaders() throws AuthFailureError{
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json" );
        headers.put("Accept","application/json");
        return headers;
    }
    @Override
    public Map<String, String> getParams() {
        return parameters;
    }

}
