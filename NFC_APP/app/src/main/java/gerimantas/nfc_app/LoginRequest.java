package gerimantas.nfc_app;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Gerimantas on 2016-04-09.
 */
public class LoginRequest extends StringRequest {

    private static String REGISTER_REQUEST_URL = "http://test:49711/token";
    private static String TAG = "LOGIN";
    private Map<String, String> parameters;

    public LoginRequest( String userName, String password, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Request.Method.POST, REGISTER_REQUEST_URL, listener, errorListener);
        parameters = new HashMap<>();
        parameters.put("username", userName);
        parameters.put("password", password);
        parameters.put("grant_type", "password");
    }

    //Log.d(TAG, userName);
    //Log.d(TAG, password);
    //Log.d(TAG, "Initialize request");
    //Log.d(TAG, "-> "+ REGISTER_REQUEST_URL);

    @Override
    public String getBodyContentType() {
        return "application/x-www-form-urlencoded";
    }

    @Override
    public Map<String, String> getParams() {
        return parameters;
    }

}
