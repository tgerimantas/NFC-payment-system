package gerimantas.nfc_app;

import java.util.Date;

/**
 * Created by Gerimantas on 2016-05-20.
 */
public class User  {

    private String firstName;
    private String lastName;
    private String id;
    private String email;


    public User (String firstName, String lastName, String email, String id){
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }
}
