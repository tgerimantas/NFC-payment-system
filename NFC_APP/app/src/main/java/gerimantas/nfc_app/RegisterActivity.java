package gerimantas.nfc_app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import android.provider.Settings.Secure;

public class RegisterActivity extends AppCompatActivity {

    private final String TAG = "RegisterActivity";
    private static String REGISTER_REQUEST_URL = "http://test:49711/api/account/create";
    private Map<String, String> parameters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        final EditText etFirstName = (EditText) findViewById(R.id.editFirstName);
        final EditText etLastName = (EditText) findViewById(R.id.editLastName);
        final EditText etUserName = (EditText) findViewById(R.id.editUserName);
        final EditText etEmail = (EditText) findViewById(R.id.editEmail);
        final EditText etPassword = (EditText) findViewById(R.id.editPassword);
        final EditText etConfirmPassword = (EditText) findViewById(R.id.editConfirmationPassword);

        final Button bRegister = (Button) findViewById(R.id.buttonRegister);

        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String firstName = etFirstName.getText().toString();
                final String lastName = etLastName.getText().toString();
                final String email = etEmail.getText().toString();
                final String userName = etUserName.getText().toString();
                final String password = etPassword.getText().toString();
                final String confirmationPassword = etConfirmPassword.getText().toString();
                String android_id = Secure.getString(getApplicationContext().getContentResolver(), Secure.ANDROID_ID);

                Log.d(TAG, firstName+ " "+lastName+ " "+email+" "+userName+" "+password);

                parameters = new HashMap<String, String>();
                parameters.put("email", email);
                parameters.put("username", userName);
                parameters.put("firstName", firstName);
                parameters.put("lastName", lastName);
                parameters.put("roleName", "User");
                parameters.put("password", password);
                parameters.put("confirmPassword", password);
                parameters.put("phoneID", android_id);
                parameters.put("grant-type", "password");



                StringRequest registerRequest = new StringRequest(Request.Method.POST, REGISTER_REQUEST_URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(RegisterActivity.this, "Klaida", Toast.LENGTH_LONG).show();
                    }
                }
                ){
                    @Override
                    public Map<String, String> getParams() {
                        return parameters;
                    }
                };
                RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
                registerRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(registerRequest);


            }
        });
    }



}
