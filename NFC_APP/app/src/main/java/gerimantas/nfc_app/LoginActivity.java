package gerimantas.nfc_app;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    private final String TAG = "LoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        final EditText etUserName = (EditText) findViewById(R.id.editUserName);
        final EditText etPassword = (EditText) findViewById(R.id.editPassword);
        final Button bLogin = (Button) findViewById(R.id.buttonLogin);
        final TextView registerLink = (TextView) findViewById(R.id.textViewRegisterHere);
        final TextView offileModeLink = (TextView) findViewById(R.id.textViewOfflineMode);

        SharedPreferences preferences = getApplicationContext().getSharedPreferences("user_data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("authorization", true);
        //preferences.edit().remove("selected_token_id");
        //preferences.edit().remove("selected_token");
        editor.commit();

        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        //Patikrinama ar mobiliajame įrenginyje yra įjungtas NFC
        NfcManager manager = (NfcManager) getApplicationContext().getSystemService(Context.NFC_SERVICE);
        NfcAdapter adapter = manager.getDefaultAdapter();
        if (adapter != null && adapter.isEnabled()) {

        }else{
            Toast.makeText(this,"NFC not connected", Toast.LENGTH_SHORT).show();
            thread.start();

        }
        //Ar įrenginys palaiko HCE technologiją.
        boolean isHceSupported = getPackageManager().hasSystemFeature("android.hardware.nfc.hce");
        if(isHceSupported == false){ //If phone is not supported HCE.
            Toast.makeText(this,"Phone doesn't support HCE", Toast.LENGTH_SHORT).show();
            thread.start();
        }



        offileModeLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences token = getSharedPreferences("user_data", MODE_PRIVATE);
                SharedPreferences.Editor editor = token.edit();
                editor.putBoolean("isOffile", true );
                editor.commit();
                Intent userIntent = new Intent(LoginActivity.this, UserActivity.class);
                LoginActivity.this.startActivity(userIntent);
            }
        });

        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                LoginActivity.this.startActivity(registerIntent);
            }
        });

        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this, 0);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Authenticating...");
                progressDialog.show();


                final String userName = etUserName.getText().toString();
                final String password = etPassword.getText().toString();

                if(true) {

                    Response.Listener<String> listener = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject object = new JSONObject(response);

                                if (object.get("access_token").toString() != null) {
                                    //Login token authentication
                                    SharedPreferences token = getSharedPreferences("user_data", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = token.edit();
                                    editor.putString("token", object.get("access_token").toString());
                                    editor.putBoolean("isOffile", false );
                                    editor.commit();

                                    progressDialog.dismiss();
                                    Intent intent = new Intent(LoginActivity.this, UserActivity.class);
                                    LoginActivity.this.startActivity(intent);

                                } else {
                                    //progressDialog.dismiss();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this); // for errors
                                    builder.setMessage("Login failed").setNegativeButton(response.toString(), null).create().show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    };

                    Response.ErrorListener errorListener = new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d(TAG, ""+error);
                            Toast.makeText(LoginActivity.this, "Ryšio klaida"+ error, Toast.LENGTH_LONG).show();
                        }
                    };

                    LoginRequest loginRequest = new LoginRequest(userName, password, listener, errorListener);
                    RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
                    loginRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    queue.add(loginRequest);
                }
            }

            private boolean validation() {
                final String userName = etUserName.getText().toString();
                final String password = etPassword.getText().toString();
                Log.d(TAG, userName.toString());
                Log.d(TAG, password.toString());

                if(userName.isEmpty()){
                    etUserName.setError("Please write username");
                    return false;
                }

                if(password.isEmpty()) {
                    etPassword.setError("Please write valid password");
                    return false;
                }
                return true;

            }
        });
    }

}
