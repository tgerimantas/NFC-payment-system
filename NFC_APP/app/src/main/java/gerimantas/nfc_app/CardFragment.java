package gerimantas.nfc_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by Gerimantas on 2016-04-15.
 */
public class CardFragment extends android.support.v4.app.Fragment {

    private RecyclerView recyclerView;
    private FragmentActivity fragmentActivity;
    private LinearLayoutManager linearLayoutManager;
    private FloatingActionButton floatingActionButton;
    private EditText add_card_ed;
    private EditText expiredDate;
    private EditText expiredMonth;
    private EditText card_cvc;
    private AlertDialog.Builder dialogBuilder;

    private View dialogView;

    private AlertDialog alertDialog;
    private String TAG = "CARD";
    private CreditCardRecyclerAdapter CCRAdapter;
    private String ADD_CARD_REQUEST_URL = "http://test:49711/api/account/addcard";

    //private PublicKey publicKey;
    //private PrivateKey privateKey;
    //This is the public key
    //private PublicKey publicKey =  "<RSAKeyValue><Modulus>uznzVPilsR1rWPkpq6m6IALaafDnVZTDDcnEyBD3A/PBx2JZTKM0DTgiTDDwCEmQBNBpPILcIBdtg3aSUgicair+2ksYrVFT+uiy0Zy1nU6qoJ+SsapLKrpCa1zHpV4LMO/pFo4Foqzw0C1FNe56FXo1xj77GPgeYl0MHUVtAUc=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
    //This is the private and public key.
    //private PrivateKey privateKey = "<RSAKeyValue><Modulus>uznzVPilsR1rWPkpq6m6IALaafDnVZTDDcnEyBD3A/PBx2JZTKM0DTgiTDDwCEmQBNBpPILcIBdtg3aSUgicair+2ksYrVFT+uiy0Zy1nU6qoJ+SsapLKrpCa1zHpV4LMO/pFo4Foqzw0C1FNe56FXo1xj77GPgeYl0MHUVtAUc=</Modulus><Exponent>AQAB</Exponent><P>+jPKs9JxpCSzNY+YNanz49Eo/A6RaU1DZWoFm/bawffZOompeL1jzpUlJUIrKVZJkNFvlxE90uXVwjxWBLv9BD==</P><Q>v5CVWKZ5Wo7W0QyoEOQS/OD8tkKS9DjzZnbnuo6lhcMaxsBrCWLstac1Xm2oFNtZgLtrPGbPfCNC5Su4Rz/P5w==</Q><DP>ZnyikmgobqEt20m3gnvcUDxT+nOJMsYYTklQhONoFj4M+EJ9bdy+Lle/gHSLM4KJ3c08VXgVh/bnSYnnfkb20Q==</DP><DQ>sSYGRfWk0W64Dpfyr7QKLxnr+Kv186zawU2CG44gWWNEVrnIAeUeWxnmi41CWw9BZH9sum2kv/pnuT/F6PWEzw==</DQ><InverseQ>XpWZQKXa1IXhF4FX3XRXVZGnIQP8YJFJlSiYx6YcdZF24Hg3+Et6CZ2/rowMFYVy+o999Y5HDC+4Qa1yWvW1vA==</InverseQ><D>Kkfb+8RrJqROKbma/3lE3xXNNQ7CL0F5CxQVrGcN8DxL9orvVdyjlJiopiwnCLgUHgIywceLjnO854Q/Zucq6ysm2ZRq36dpGLOao9eg+Qe8pYYO70oOkEe1HJCtP1Laq+f3YK7vCq7GkgvKAI9uzOd1vjQv7tIwTIADK19ObgE=</D></RSAKeyValue>";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_card, parent, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerCardList);
        fragmentActivity = getActivity();

        linearLayoutManager = new LinearLayoutManager(fragmentActivity);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);


        //generateKeys(); //GENERATES PUBLIC PRIVATE KEYS
        //SharedPreferences preferences = getActivity().getSharedPreferences("user_data", Context.MODE_PRIVATE);
        //preferences.edit().remove("cards").commit();
       // preferences.edit().remove("selected_token").commit();

        SharedPreferences preferences = getContext().getSharedPreferences("user_data", Context.MODE_PRIVATE);
        boolean isOffile = preferences.getBoolean("isOffile", true);
        final List<Card> cards = getCards();
        List<Card> updateCards =  new ArrayList<Card>();

        final Card[] updateCard = {null};
        if(isOffile != true){

            for (int i = 0; i < cards.size(); i++) {
                if(cards.get(i).getTimesToUse() <= 0){
//                    final int finalI = i;
//                    Response.Listener<String> listener = new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            try {
//
//                                JSONObject object = new JSONObject(response);
//
//                                if(  object.isNull("error") ){
//                                    updateCard[0] = cards.get(finalI);
//                                    updateCard[0].setTimesToUse(5);
//                                    updateCard[0].setToken(object.get("token").toString());
//                                    setList(updateCard[0]);
//
//                                }else{
//                                    Toast.makeText(getActivity().getApplicationContext(), "Error: "+ object.get("error").toString(), Toast.LENGTH_LONG).show();
//                                }
//                            } catch (JSONException e) {
//
//                                e.printStackTrace();
//                            }
//                        }
//                    };
//
//                    Response.ErrorListener errorListener = new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            Log.d(TAG, ""+error);
//                            Toast.makeText(getActivity().getApplicationContext(), "Ryšio klaida"+ error, Toast.LENGTH_LONG).show();
//                        }
//                    };
//
//
//                    CardUpdateRequest cardRequest = new CardUpdateRequest(getActivity(), String.valueOf(cards.get(i).getId()), listener, errorListener, getActivity().getApplicationContext());
//                    RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
//                    cardRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//                    queue.add(cardRequest);
                    cards.get(i).setTimesToUse(5);
                    updateCards.add(cards.get(i));
                }else{
                    updateCards.add(cards.get(i));
                }
            }
        }

        if(updateCards == null){
            updateCards = cards;
        }else{

            String cardsJson = preferences.getString("cards", "");
            Gson gson = new Gson();
            SharedPreferences.Editor editor = preferences.edit();
            String json = gson.toJson(updateCards);
            editor.putString("cards", json);
            editor.commit();
        }

        if(updateCards != null){
            CCRAdapter = new CreditCardRecyclerAdapter(cards, getContext());
            recyclerView.setAdapter(CCRAdapter); // put credit cards here
        }
        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.add_credit_card_fab);

        if(isOffile == true){
            floatingActionButton.setVisibility(View.GONE);
        }


        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                dialogBuilder = new AlertDialog.Builder(view.getContext());
                dialogView = LayoutInflater.from(view.getContext()).inflate(R.layout.add_credit_card_dialog_box, null);
                dialogBuilder.setView(dialogView);

                add_card_ed = (EditText) dialogView.findViewById(R.id.add_credit_card_number);
                //final String[] selectedBank = new String[1];
                //Spinner dropdown = (Spinner)dialogView.findViewById(R.id.dropdownBankList);
                //final String[] items = new String[]{"SEB bankas", "Swedbank bankas ", "DNB bankas"};
                //ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_dropdown_item, items);
                //dropdown.setAdapter(adapter);

//                dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                    @Override
//                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                        selectedBank[0] = items[position];
//                        Log.v("item", (String) parent.getItemAtPosition(position));
//                    }
//
//                    @Override
//                    public void onNothingSelected(AdapterView<?> parent) {
//
//                    }
//                });


                dialogBuilder.setPositiveButton("Pridėti", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        final ProgressDialog progressDialog = new ProgressDialog(getActivity(), 0);
                        progressDialog.setIndeterminate(true);
                        progressDialog.setMessage("Pridedama...");
                        progressDialog.show();


                        if (!add_card_ed.getText().toString().isEmpty()) {
                            CreditCard validation = new CreditCard();
                            boolean valid = validation.isValid(add_card_ed.getText().toString());
                            if (valid == true) {
                                String cardType = String.valueOf(CreditCard.CardType.detect(add_card_ed.getText().toString()));
                                //Toast.makeText(getActivity().getApplicationContext(), "Valid" + cardType, Toast.LENGTH_LONG).show();

                                Response.Listener<String> listener = new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try { //PUSH card to list with token
                                            JSONObject object = new JSONObject(response);
                                            Log.d("RESPONSE->ADD CARD: ", response);
                                            if(  object.isNull("error") ){
                                                SharedPreferences preferences = getActivity().getSharedPreferences("user_data", Context.MODE_PRIVATE);
                                                SharedPreferences.Editor editor = preferences.edit();
                                                String cardsJson = preferences.getString("cards", "");

                                                Gson gson = new Gson();
                                                boolean cardListIsNull = false;
                                                ArrayList<Card> objects = new ArrayList<Card>();

                                                Type type = new TypeToken<ArrayList<Card>>(){}.getType();
                                                objects = (ArrayList<Card>) gson.fromJson(cardsJson, type);
                                                if(objects == null){
                                                    cardListIsNull = true;
                                                    objects = new ArrayList<Card>();
                                                }

                                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                                Date date = null;
                                                try {
                                                    date = (Date)formatter.parse(object.get("expired").toString());
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }

                                                String cardNumber = add_card_ed.getText().toString();
                                                String changedCardNumber = cardNumber;
                                                switch (cardNumber.length()){
                                                    case 11:
                                                        changedCardNumber = "**** **** *** " + cardNumber.substring(cardNumber.length() - 4);
                                                        break;
                                                    case 16:
                                                        changedCardNumber = "**** **** **** **** " + cardNumber.substring(cardNumber.length() - 4);
                                                        break;
                                                }


                                                objects.add(new Card( object.get("token").toString(), object.get("bank").toString(), date, Integer.parseInt(object.get("id").toString()), object.get("userid").toString(), changedCardNumber, 5 )); //ADD NEW OBJECT TO LIST

                                                //PUSH LIST OF OBJECTS TO JSON AND SAVE IN SHARED PREFERENCES
                                                String json = gson.toJson(objects);
                                                editor.putString("cards", json);
                                                editor.commit();

                                                progressDialog.dismiss();
                                                //UPDATE LIST IN FRAGMENT
                                                recyclerView.setAdapter(new CreditCardRecyclerAdapter(getCards(), getContext()));
                                               // if(cardListIsNull == true){
                                                 //   CCRAdapter.notifyDataSetChanged();
                                                //}

                                            }else{
                                                progressDialog.dismiss();
                                                Toast.makeText(getActivity().getApplicationContext(), "Error: "+ object.get("error").toString(), Toast.LENGTH_LONG).show();
                                            }
                                        } catch (JSONException e) {
                                            progressDialog.dismiss();
                                            e.printStackTrace();
                                        }
                                    }
                                };

                                Response.ErrorListener errorListener = new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.d(TAG, ""+error);
                                        progressDialog.dismiss();
                                        Toast.makeText(getActivity().getApplicationContext(), "Ryšio klaida"+ error, Toast.LENGTH_LONG).show();
                                    }
                                };


                                    CardRequest cardRequest = new CardRequest(getActivity(), add_card_ed.getText().toString(), listener, errorListener, getActivity().getApplicationContext());
                                    RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
                                    cardRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                    queue.add(cardRequest);

                                    //Toast.makeText(getActivity().getApplicationContext(), "Error bad encryption", Toast.LENGTH_LONG).show();


                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(getActivity().getApplicationContext(), "Not Valid", Toast.LENGTH_LONG).show();
                            }
                        }else{
                            progressDialog.dismiss();
                            Toast.makeText(getActivity().getApplicationContext(), "Error -> Input no card number", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                dialogBuilder.setNegativeButton("Atmesti", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });

                alertDialog = dialogBuilder.create();
                alertDialog.show();
            }
        });

    }

//    private void setList(Card card) {
//        updateCards.add(card);
//    }


    private List<Card> getCards() {

        SharedPreferences preferences = getActivity().getSharedPreferences("user_data", Context.MODE_PRIVATE);
        String cardsJson = preferences.getString("cards", "");
        String user = preferences.getString("user", "");

        Gson gson = new Gson();
        Type typeUser = new TypeToken<User>(){}.getType();
        User userData = (User) gson.fromJson(user, typeUser);

        ArrayList<Card> userCards =  new ArrayList<Card>();

        Type type = new TypeToken<ArrayList<Card>>(){}.getType();
        ArrayList<Card> objects = (ArrayList<Card>) gson.fromJson(cardsJson, type);
        if(objects != null) {
            for (int i = 0; i < objects.size(); i++) {
                if (objects.get(i).getUserId().equals(userData.getId())) {
                    userCards.add(objects.get(i));
                }
            }
        }
        return userCards;
    }


    //GENERATES PUBLIC PRIVATE KEY
//    public void generateKeys(){
//        try {
//            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
//            generator.initialize(1024);
//            KeyPair keyPair = generator.generateKeyPair();
//            publicKey = keyPair.getPublic();
//            privateKey = keyPair.getPrivate();
//
//            Log.d("Public key", publicKey.toString());
//            Log.d("Private key", privateKey.toString());
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//
//    }




}
