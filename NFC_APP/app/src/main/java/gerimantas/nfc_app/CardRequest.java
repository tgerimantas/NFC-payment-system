package gerimantas.nfc_app;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Gerimantas on 2016-05-01.
 */
public class CardRequest extends StringRequest {

    private static String ADD_CARD_REQUEST_URL = "http://test:49711/api/account/addcard";
    private static String TAG = "ADD CARD";
    private Map<String, String> parameters;
    private Context context;

    public CardRequest(Context context1, String data, Response.Listener<String> listener, Response.ErrorListener errorListener, Context context) {
        super(Method.POST, ADD_CARD_REQUEST_URL, listener, errorListener);
        this.context = context1;
        Log.d(TAG, "Initialize request");
        Log.d(TAG, "-> "+ ADD_CARD_REQUEST_URL);
        Log.d(TAG, "-> "+ data);
        parameters = new HashMap<String, String>();
        parameters.put("CardNumber", data);
        //parameters.put("encryptedKey", encryptedKey);
    }
    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        SharedPreferences preferences = context.getSharedPreferences("user_data", Context.MODE_PRIVATE);
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Accept","application/json");
        headers.put("Authorization", "Bearer " + preferences.getString("token", null));
        return headers;
    }
    @Override
    public Map<String, String> getParams() {
        return parameters;
    }


    //Log.d(TAG, "-> "+ preferences.getString("token", null));
    //headers.put("Content-Type", "application/json; charset=utf-8" );
}
