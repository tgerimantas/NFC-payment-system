package gerimantas.nfc_app;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Gerimantas on 2016-05-17.
 */
public class UserRequest extends StringRequest {
    private static String USER_REQUEST_URL = "http://test:49711/api/account/user";
    private static String TAG = "USER";
    private Map<String, String> parameters;
    private Context context;

    public UserRequest(Context context,  Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.GET, USER_REQUEST_URL, listener, errorListener);
        this.context = context;
    }
    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        SharedPreferences preferences = context.getSharedPreferences("user_data", Context.MODE_PRIVATE);
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Accept","application/json");
        headers.put("Authorization", "Bearer " + preferences.getString("token", null));
        return headers;
    }

}
