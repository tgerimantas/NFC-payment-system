﻿using System;
using System.Windows.Forms;

namespace POS
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.createBooking = new System.Windows.Forms.Button();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.systemLogtextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dateToTimePicker = new System.Windows.Forms.DateTimePicker();
            this.dateFromTimePicker = new System.Windows.Forms.DateTimePicker();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.addressTextBox = new System.Windows.Forms.TextBox();
            this.phoneNumberTextBox = new System.Windows.Forms.TextBox();
            this.roomsDropBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelPrice = new System.Windows.Forms.Label();
            this.find_rooms = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // createBooking
            // 
            this.createBooking.Location = new System.Drawing.Point(261, 235);
            this.createBooking.Name = "createBooking";
            this.createBooking.Size = new System.Drawing.Size(200, 41);
            this.createBooking.TabIndex = 0;
            this.createBooking.Text = "Rezervuoti";
            this.createBooking.UseVisualStyleBackColor = true;
            this.createBooking.Click += new System.EventHandler(this.button_create);
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Location = new System.Drawing.Point(12, 74);
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(140, 20);
            this.firstNameTextBox.TabIndex = 1;
            this.firstNameTextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label1.Location = new System.Drawing.Point(185, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(382, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Viešbučių atsiskaitymo sistemos prototipas";
            // 
            // systemLogtextBox
            // 
            this.systemLogtextBox.Location = new System.Drawing.Point(12, 296);
            this.systemLogtextBox.Multiline = true;
            this.systemLogtextBox.Name = "systemLogtextBox";
            this.systemLogtextBox.Size = new System.Drawing.Size(670, 183);
            this.systemLogtextBox.TabIndex = 3;
            this.systemLogtextBox.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Vardas";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(187, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Pavardė";
            // 
            // dateToTimePicker
            //
            this.dateToTimePicker.Format = DateTimePickerFormat.Custom;
            this.dateToTimePicker.CustomFormat = "yyyy-MM-dd";
            this.dateToTimePicker.Location = new System.Drawing.Point(261, 151);
            this.dateToTimePicker.Name = "dateToTimePicker";
            this.dateToTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dateToTimePicker.TabIndex = 10;
            this.dateToTimePicker.ValueChanged += new System.EventHandler(this.dateToTimePicker_ValueChanged);
            // 
            // dateFromTimePicker
            // 
           
            this.dateFromTimePicker.Format = DateTimePickerFormat.Custom;
            this.dateFromTimePicker.CustomFormat = "yyyy-MM-dd";
            this.dateFromTimePicker.MinDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            this.dateFromTimePicker.Location = new System.Drawing.Point(12, 151);
            this.dateFromTimePicker.Name = "dateFromTimePicker";
            this.dateFromTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dateFromTimePicker.TabIndex = 11;
            this.dateFromTimePicker.ValueChanged += new System.EventHandler(this.dateFromTimePicker_ValueChanged);
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.Location = new System.Drawing.Point(190, 74);
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.Size = new System.Drawing.Size(139, 20);
            this.lastNameTextBox.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(368, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Address";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(539, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Telefono numeris";
            // 
            // addressTextBox
            // 
            this.addressTextBox.Location = new System.Drawing.Point(371, 74);
            this.addressTextBox.Name = "addressTextBox";
            this.addressTextBox.Size = new System.Drawing.Size(140, 20);
            this.addressTextBox.TabIndex = 15;
            // 
            // phoneNumberTextBox
            // 
            this.phoneNumberTextBox.Location = new System.Drawing.Point(542, 74);
            this.phoneNumberTextBox.Name = "phoneNumberTextBox";
            this.phoneNumberTextBox.Size = new System.Drawing.Size(140, 20);
            this.phoneNumberTextBox.TabIndex = 16;
            // 
            // roomsDropBox
            // 
            this.roomsDropBox.FormattingEnabled = true;
            this.roomsDropBox.Location = new System.Drawing.Point(512, 150);
            this.roomsDropBox.Name = "roomsDropBox";
            this.roomsDropBox.Size = new System.Drawing.Size(121, 21);
            this.roomsDropBox.TabIndex = 17;
            this.roomsDropBox.SelectedIndexChanged += new System.EventHandler(this.roomsDropBox_SelectedIndexChanged_1);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 108);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Rezervavimas";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 135);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Nuo";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(258, 135);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(18, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Iki";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(509, 135);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Kambariai nr.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(509, 183);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Kaina";
            // 
            // labelPrice
            // 
            this.labelPrice.AutoSize = true;
            this.labelPrice.Location = new System.Drawing.Point(574, 183);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(13, 13);
            this.labelPrice.TabIndex = 13;
            this.labelPrice.Text = "0";
            // 
            // find_rooms
            // 
            this.find_rooms.Location = new System.Drawing.Point(12, 178);
            this.find_rooms.Name = "find_rooms";
            this.find_rooms.Size = new System.Drawing.Size(75, 23);
            this.find_rooms.TabIndex = 24;
            this.find_rooms.Text = "Ieškoti kamb.";
            this.find_rooms.UseVisualStyleBackColor = true;
            this.find_rooms.Click += new System.EventHandler(this.find_rooms_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 491);
            this.Controls.Add(this.find_rooms);
            this.Controls.Add(this.labelPrice);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.roomsDropBox);
            this.Controls.Add(this.phoneNumberTextBox);
            this.Controls.Add(this.addressTextBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lastNameTextBox);
            this.Controls.Add(this.dateFromTimePicker);
            this.Controls.Add(this.dateToTimePicker);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.systemLogtextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.firstNameTextBox);
            this.Controls.Add(this.createBooking);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button createBooking;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox systemLogtextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateToTimePicker;
        private System.Windows.Forms.DateTimePicker dateFromTimePicker;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox addressTextBox;
        private System.Windows.Forms.TextBox phoneNumberTextBox;
        private System.Windows.Forms.ComboBox roomsDropBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelPrice;
        private System.Windows.Forms.Button find_rooms;
    }
}

