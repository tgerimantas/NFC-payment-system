﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS
{
    public class RoomData
    {
        public string Error { get; set; }
        public string Accept { get; set; }
        public List<Room> Rooms { get; set; }
    }
}
