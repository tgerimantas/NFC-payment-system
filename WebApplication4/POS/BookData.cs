﻿
namespace POS
{
    public class BookData
    {
        public int Room { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string Token { get; set; }
        public string UserId { get; set; }
        public string PhoneId { get; set; }
    } 
}
