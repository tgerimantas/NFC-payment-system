﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POS
{

    public partial class Form1 : Form
    {
       
        public string URL = "http://localhost:60376/"; //Galimybė prisijungti prie vidinės viešbučio serviso
        SerialPort serialPort = new SerialPort();
        private bool exePayment = false;
        private BookData bookData;
        private List<Room> rooms;


        public Form1()
        {
            InitializeComponent();        
            serialPort.DataReceived += new SerialDataReceivedEventHandler(ReceivingData);
        }

        //Pakraunamas kambarių sąrašas
        private async void LoadRooms(DateTime dateFrom, DateTime dateTo)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var dateData = new DateTimePicker() { DateFrom = dateFrom, DateTo = dateTo };

                HttpResponseMessage response = await client.PostAsync("/api/pos/rooms",
                      new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(dateData), Encoding.UTF8, "application/json"));

                var dataJson = response.Content.ReadAsStringAsync();
                dataJson.Wait();
                RoomData roomData = JsonConvert.DeserializeObject<RoomData>(dataJson.Result);
                if (response.IsSuccessStatusCode)
                {
                    if(roomData.Rooms != null) 
                    {
                        rooms = roomData.Rooms.ToList();
                        roomsDropBox.Items.Clear();
                        for (int i = 0; i < roomData.Rooms.Count; i++)
                        {
                            roomsDropBox.Items.Add(rooms[i].RoomNumber);
                        }                                             
                    }
                    else{
                        MessageBox.Show(roomData.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Error: Bad request (find rooms) ");
               }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var dateFrom = DateTime.Parse( DateTime.Now.ToString("yyyy-MM-dd"));
            var dateTo = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
           
            LoadRooms(dateFrom, dateTo);
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            InitSerialPort();

            if (serialPort != null && serialPort.IsOpen)
            {
                serialPort.Close();
            }
            serialPort.Open();
        }

        //Init Arduino 
        public void InitSerialPort()
        {
            serialPort.PortName = "COM3";
            serialPort.BaudRate = 115200;
            serialPort.DtrEnable = true; 
        }

        //Gija, kuri naudojama gauti duomenis iš Arduino per serial Port        
        void ReceivingData(object sender, SerialDataReceivedEventArgs e)
        {
           
            Thread.Sleep(500);
            while (serialPort.IsOpen && serialPort.BytesToRead > 0)
            {
                try
                {
                    string data = serialPort.ReadLine();
                    this.Invoke((MethodInvoker)async delegate ()
                    {
                        if (data.Length > 2)
                        {
                            if (data != null)
                            {

                                systemLogtextBox.Text += data + "\n";

                                data = data.Replace("\r\n", "").Replace("\r", "").Replace("\n", "");
                                if (exePayment == true)
                                {
                                    exePayment = false;
                                    try
                                    {
                                        string[] paymentData = data.Split(':');
                                        bookData.UserId = paymentData[0];
                                        bookData.Token = paymentData[1];
                                        bookData.PhoneId = paymentData[2];
                                        await CreatePayment(bookData);
                                    }
                                    catch (Exception)
                                    {
                                        systemLogtextBox.Text += "ERROR DATA SPLIT" + "\n";
                                    }

                                }

                                if (data.Equals("exePayment"))
                                {
                                    exePayment = true;
                                }
                            }
                        }

                    });
                }
                catch (TimeoutException) { }
            }
        }

        //Siunčiama komanda Arduinui, kad jinai lauktų vartotojo mobiliojo įrenginio iš kurio bus gaunama mokėjimo žetonas.  
        private void MakePayment()
        {
            if (serialPort.IsOpen)
            {
                serialPort.WriteLine("1"); 
                Thread.Sleep(500);
            }
        }

        //Siunčiama užklausą į vidinę viešbučio sistemą
        private async Task CreatePayment(BookData data)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.PostAsync("/api/pos/pay",
                    new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json"));

                var dataJson = response.Content.ReadAsStringAsync();
                dataJson.Wait();
                PaymentResponseData paymentResponseData = JsonConvert.DeserializeObject<PaymentResponseData>(dataJson.Result);
                if (response.IsSuccessStatusCode)
                {
                    if (paymentResponseData.Error == null)
                    {
                        MessageBox.Show(paymentResponseData.Accept);
                        //Sugeneruotas žetonas yra persiunčiamas į mobilųjį įrenginį
                        SendRoomKey(paymentResponseData.Token);
                    }
                    else
                    {
                        MessageBox.Show(paymentResponseData.Error);
                    }
                  
                }
                else
                {
                    MessageBox.Show("Hotel service error");

                }
            }
        }

        //Siunčiama komanda ir unikalaus vartotojo raktas
        private void SendRoomKey(string token)
        {
            if (serialPort.IsOpen)
            {
                string data = "2"+":"+token; //Command + data(token)
                serialPort.WriteLine(data); 
                
                Thread.Sleep(500);
            }
        }
      


        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            systemLogtextBox.ScrollBars = ScrollBars.Both;
            systemLogtextBox.SelectionStart = systemLogtextBox.Text.Length;
            systemLogtextBox.ScrollToCaret();
        }

        private void button_cancel(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.WriteLine("CANCEL");
              
                Thread.Sleep(500);
            }
        }

        //Paspaudus mygtuką visi duomenys iš įvedimo laukų gaunami ir sudaromas objektas
        private void button_create(object sender, EventArgs e)
        {
            var firstName = firstNameTextBox.Text;
            var lastName = lastNameTextBox.Text;
            var address = addressTextBox.Text;
            var phoneNumber = phoneNumberTextBox.Text;
            var dateFrom = dateFromTimePicker.Text;
            var dateTo = dateToTimePicker.Text;
            var room = rooms[roomsDropBox.SelectedIndex].Id;

            bookData = new BookData()
            {
                Room = room,
                FirstName = firstName,
                LastName = lastName,
                Address = address,
                PhoneNumber = phoneNumber,
                DateFrom = dateFrom,
                DateTo = dateTo,
                
            };
            MakePayment();
        }

        private void roomsDropBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            
            labelPrice.Text = rooms[roomsDropBox.SelectedIndex].Price.ToString();      
        }

        private void dateFromTimePicker_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dateToTimePicker_ValueChanged(object sender, EventArgs e)
        {

        }


        private void find_rooms_Click(object sender, EventArgs e)
        {
            var dateFrom = DateTime.ParseExact( dateFromTimePicker.Text.ToString(), "yyyy-MM-dd", null);
            var dateTo = DateTime.ParseExact(dateToTimePicker.Text.ToString(), "yyyy-MM-dd", null); 

            //if(dateTo <= DateTime.Now)
            //{
            //    MessageBox.Show("Negalima pasirinkti ankstenės datos dateTo");
            //}

            //if (dateFrom <= DateTime.Now)
            //{
            //    MessageBox.Show("Negalima pasirinkti ankstenės datos dateFrom");
            //}

            //if (dateTo <= dateFrom)
            //{
            //    MessageBox.Show("Negalima pasirinkti datos dateTo<dateFrom");
            //}

            LoadRooms(dateFrom, dateTo);

        }
    }
}
