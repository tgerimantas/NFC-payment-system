﻿
namespace POS
{
    public class Room
    {
        
        public int Id { get; set; }       
        public int HotelId { get; set; }       
        public int Floor { get; set; }     
        public int RoomNumber { get; set; }      
        public float Price { get; set; }     
        public int RoomTypeId { get; set; }
    }
}
