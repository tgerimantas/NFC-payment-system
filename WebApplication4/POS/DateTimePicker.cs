﻿using System;

namespace POS
{
    class DateTimePicker
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}
