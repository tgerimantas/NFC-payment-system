﻿namespace POS
{
    public class PaymentResponseData
    {
        public string Error { get; set; }
        public string Accept { get; set; }
        public string Token { get; set; }
    }
}