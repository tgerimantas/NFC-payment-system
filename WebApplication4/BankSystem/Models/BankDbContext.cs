namespace BankSystem.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;

    public class BankDbContext : DbContext
    {
   
        public BankDbContext()
            : base("name=BankDbContext")
        {
        }

        public DbSet<User> User { get; set; }
        public DbSet<Bank> Bank { get; set; }
        public DbSet<PaymentCard> PaymentCard { get; set; }
        public DbSet<PaymentCardType> PaymentCardType { get; set; }
        public DbSet<Transaction> Transaction { get; set; }
       
    }


    [Table("USER")]
    public class User
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("FIRST_NAME")]
        public string FirstName { get; set; }
        [Column("LAST_NAME")]
        public string LastName { get; set; }
        [Column("ADDRESS")]
        public string Address { get; set; }
        [Column("EMAIL")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public IEnumerable<PaymentCard> PaymentCard { get; set; }
    }


    /// <summary>
    /// Bank Section Entities
    /// </summary>

    [Table("BANK")]
    public class Bank
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("NAME")]
        public string Name { get; set; }
        [Column("ADDRESS")]
        public string Address { get; set; }
        [Column("WEBSITE_ADDRESS")]
        public string Website_Address { get; set; }
        [Column("COMPANY_MAIL_ADDRESS")]
        public string CompanyMailAddress { get; set; }

        public IEnumerable<PaymentCard> PaymentCard { get; set; }
    }


    [Table("TRANSACTION")]
    public class Transaction
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("PAYMENT")]
        public float Payment { get; set; }
        [Column("SENDER_CARD_ID")]
        [ForeignKey("PaymentCard")]
        public int SenderCardId { get; set; }
        [Column("RECIPIENT_CARD_ID")]
        public int RecipientCardId { get; set; }
       
        public PaymentCard PaymentCard { get; set; }
    }

    
    [Table("PAYMENT_CARD")]
    public class PaymentCard
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("CARD_NUMBER")]
        public string CardNumber { get; set; }
        [Column("BANK_ID")]
        [ForeignKey("Bank")]
        public int BankId { get; set; }
        [Column("User_ID")]
        [ForeignKey("User")]
        public int UserId { get; set; }
        [Column("TYPE_ID")]
        [ForeignKey("PaymentCardType")]
        public int TypeId { get; set; }
        [Column("BALANCE")]
        public float Balance { get; set; }
        [Column("EXPIRED")]
        public DateTime Expired { get; set; }

        public User User { get; set; }
        public Bank Bank { get; set; }
        public PaymentCardType PaymentCardType { get; set; }
        public IEnumerable<Transaction> Transaction { get; set; }
    }

    [Table("PAYMENT_CARD_TYPE")]
    public class PaymentCardType
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("NAME")]
        public string Name { get; set; }

        public IEnumerable<PaymentCard> PaymentCard { get; set; }
    }


}