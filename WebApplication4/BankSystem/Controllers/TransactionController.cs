﻿using BankSystem.Models;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace BankSystem.Controllers
{

    public class PaymentData {
        public string RecipientCardNumber { get; set; }
        public string SenderCardNumber { get; set; }
        public float Payment { get; set; }
    }

    public class PaymentStatus
    {
        public string Error { get; set; }
        public string Accept { get; set; }
    }

    [RoutePrefix("api/transaction")]
    public class TransactionController : ApiController
    {
        //Vykdomas mokėjimas (transakcija)
        //Duomenys - Kaina, Siuntėjo, gavėjo banko numeriai.
        //Įvykdomas arba neįvykdomas mokėjimas (nėra pinigų arba nėra tokios mokėjimo kortelės)
        [Route("bank")]
        [HttpPost]
        public async Task<IHttpActionResult> Payment(PaymentData data)
        {

            using (var cnt = new BankDbContext())
            {
                if ((data.RecipientCardNumber != null) && (data.SenderCardNumber  != null))
                {
                    var sender = cnt.PaymentCard.FirstOrDefault(t => t.CardNumber == data.SenderCardNumber.ToString());
                    var recipient = cnt.PaymentCard.FirstOrDefault(t => t.CardNumber == data.RecipientCardNumber.ToString());
                    if (sender != null && recipient != null)
                    {
                        if (sender.Balance >= data.Payment)
                        {

                            sender.Balance = sender.Balance - data.Payment;
                            recipient.Balance = recipient.Balance + data.Payment;
                            await cnt.SaveChangesAsync();

                            var transactionSender = new Transaction()
                            {
                                SenderCardId = sender.Id,
                                RecipientCardId = recipient.Id,
                                Payment = data.Payment
                            };

                            cnt.Transaction.Add(transactionSender);
                            await cnt.SaveChangesAsync();
                            return Ok(new PaymentStatus() {Error = null, Accept = "Transaction completed"});
                        }
                        else
                        {
                            return Ok(new PaymentStatus() {Error = "Transaction failed : No money", Accept = null});
                        }
                    }
                    else
                    {
                        return Ok(new PaymentStatus() { Error = "No recipient or sender object", Accept = null });
                    }
                   
                }
                else
                {
                    return Ok(new PaymentStatus() {  Error = "Transaction failed : No payment card",  Accept = null  });
                }
            }
        }
    }
}
