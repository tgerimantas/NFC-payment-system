﻿using BankSystem.Models;
using System;
using System.Linq;
using System.Web.Http;

namespace BankSystem.Controllers
{
    public class CardData{
        public string CardNumber { get; set; }
    }

    public class CardStatus
    {
        public DateTime Expired { get; set; }
        public string Bank { get; set; }
        public string Error { get; set; }
        public string Accept { get; set; }
    }

    [RoutePrefix("api/card")]
    public class CardController : ApiController
    {

        //Patikrina ar įvestas kortelės numeris iš mobiliojo įrenginio egzistuoja Banko duomenų bazėje.
        //Duomenys vartotojo įvesta mokėjimo kortelės numeris
        //Grąžinamas patvirtinimas arba klaida (neegiztuoja tokia mokėjimo kortelė)
        [Route("check")]
        [HttpPost]
        public  IHttpActionResult CheckPaymentCard(CardData data) {

            using (var cnt = new BankDbContext() )
            {
                if (data.CardNumber != null)
                {
                    var paymentCard = cnt.PaymentCard.FirstOrDefault(t => t.CardNumber == data.CardNumber);

                    if (paymentCard != null) // true
                    { 
                        var bank = cnt.Bank.FirstOrDefault(t => t.Id == paymentCard.BankId);
                        return Ok(bank != null ? new CardStatus() { Error = null, Accept = "Selected card is exist in database", Bank = bank.Name, Expired = paymentCard.Expired } : new CardStatus() { Error = "No bank", Accept = null, Bank = null });
                    }
                    else
                    {
                        return Ok(new CardStatus() { Error = "Selected card doesnt exist", Accept = null, Bank = null });
                    }
                }
                else
                {
                    return Ok(new CardStatus() { Error = "No payment card", Accept = null, Bank = null });
                }          
            }
        }
    }
}
