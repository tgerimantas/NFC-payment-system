namespace HotelSystem.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;

    public class HotelDbContext : DbContext
    {

        public HotelDbContext()
            : base("name=HotelDbContext")
        {
        }
        public DbSet<User> User { get; set; }
        public DbSet<Hotel> Hotel { get; set; }
        public DbSet<Room> Room { get; set; }
        public DbSet<RoomType> RoomType { get; set; }
        public DbSet<Booking> Booking { get; set; }
        public DbSet<BookingRoom> BookingRoom { get; set; }

    }

    /// <summary>
    /// Hotel section Entities
    /// </summary>
    /// 

    [Table("HOTEL")]
    public class Hotel
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("HOTEL_CODE")]
        public int HotelCode { get; set; }
        [Column("NAME")]
        public string Name { get; set; }
        [Column("ADDRESS")]
        public string Address { get; set; }
        [Column("PHONE_NUMBER")]
        public string PhoneNumber { get; set; }
        [Column("WEBSITE_ADDRESS")]
        public string Website_Address { get; set; }
        [Column("COMPANY_MAIL_ADDRESS")]
        public string CompanyMailAddress { get; set; }

        public IEnumerable<Room> Room { get; set; }
        public IEnumerable<Booking> Booking { get; set; }
    }

    [Table("ROOM")]
    public class Room
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("HOTEL_ID")]
        [ForeignKey("Hotel")]
        public int HotelId { get; set; }
        [Column("FLOOR")]
        public int Floor { get; set; }
        [Column("ROOM_NUMBER")]
        public int RoomNumber { get; set; }
        [Column("PRICE")]
        public float Price { get; set; }
        [Column("ROOM_TYPE_ID")]
        [ForeignKey("RoomType")]
        public int RoomTypeId { get; set; }

        public Hotel Hotel { get; set; }
        public RoomType RoomType { get; set; }
        public IEnumerable<BookingRoom> BookingRoom { get; set; }

    }

    [Table("ROOM_TYPE")]
    public class RoomType
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("TYPE_NAME")]
        public string TypeName { get; set; }

        public IEnumerable<Room> Room { get; set; }
    }

    [Table("BOOKING")]
    public class Booking
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("USER_ID")]
        [ForeignKey("User")]
        public int UserId { get; set; }
        [Column("PAYMENT")]
        public float Payment { get; set; }
        [Column("TOKEN")]
        public string Token { get; set; }
        [Column("HOTEL_ID")]
        [ForeignKey("Hotel")]
        public int HotelId { get; set; }

        public User User { get; set; }
        public Hotel Hotel { get; set; }
        public IEnumerable<BookingRoom> BookingRoom { get; set; }
    }

    [Table("BOOKING_ROOM")]
    public class BookingRoom
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("ROOM_ID")]
        public int RoomId { get; set; }
        [Column("BOOKING_ID")]
        [ForeignKey("Booking")]
        public int BookingId { get; set; }
        [Column("DATE_FROM")]
        public DateTime DateFrom { get; set; }
        [Column("DATE_TO")]
        public DateTime DateTo { get; set; }

        public Booking Booking { get; set; }
    }

    /// <summary>
    /// User section Entities
    /// </summary>
    [Table("USER")]
    public class User
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("FIRST_NAME")]
        public string FirstName { get; set; }
        [Column("LAST_NAME")]
        public string LastName { get; set; }
        [Column("ADDRESS")]
        public string Address { get; set; }
        [Column("PHONE_NUMBER")]
        public string PhoneNumber { get; set; }
        [Column("EMAIL")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

       
        public IEnumerable<Booking> Booking { get; set; }
    }
}