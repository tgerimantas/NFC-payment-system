﻿using HotelSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace HotelSystem.Controllers
{
 
    public class TokenData
    {
        public string Token { get; set; }
        public string HotelPaymentNumber { get; set; }
        public float Price { get; set; }
        public string PhoneId { get; set; }
        public string UserId { get; set; }
    }

    public class BookData
    {
        public int Room { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string Token { get; set; }
        public string UserId { get; set; }
        public string PhoneId { get; set; }
    }
    public class PaymentCardData
    {
        public string Error { get; set; }
        public string Accept { get; set; }
        public string Token { get;  set; }
    }

    public class DateTimePicker
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }

    public class RoomData
    {
        public string Error { get; set; }
        public string Accept { get; set; }
        public List<Room> Rooms { get; set; }
    }

    [RoutePrefix("api/pos")]
    public class PosController : ApiController
    {

        private string URL = "http://localhost:49711/";

        //Gaunami visi viešbučio kambariai
        [Route("rooms")]
        [HttpPost]
        public IHttpActionResult GetAvailableRooms(DateTimePicker dateData)
        {

            var selectedHotel = 1; //Testavimui pasirenkamas pirmutinis viešbutis iš duomenų bazės

            using (var cnt= new HotelDbContext())
            {
                var allRooms = cnt.Room.Where(t => (t.HotelId == selectedHotel)).ToList();
                var roomsBookingToRemove = cnt.BookingRoom.Where(tt =>( tt.DateTo <= dateData.DateTo) && (tt.DateFrom >= dateData.DateFrom) ).ToList();
                var rooms = allRooms.Where( t=> roomsBookingToRemove.All(tt => tt.RoomId != t.Id)).ToList();
                return Ok(new RoomData() { Accept="Availabe rooms: "+rooms.Count, Error = null, Rooms = rooms  });
            }
       
        }

        //Vykdomas mokėjimo veiksmas

        [Route("pay")]
        [HttpPost]
        public async Task<IHttpActionResult> Payment(BookData data)
        {
            var cnt = new HotelDbContext();

            using (var client = new HttpClient())
            {
                string hotelPaymentNumber = "123456789"; // Testas viešbučio mokėjimo kortelės numeris
                int hotelId = 1;        // Testas pasirinktas pirmutinis iš sąrašo viešbutis duomenų bazėje
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var room = cnt.Room.FirstOrDefault(t => t.HotelId == hotelId && t.Id == data.Room);
                if (room != null)
                {
                    var tokenData = new TokenData() { Token = data.Token, HotelPaymentNumber = hotelPaymentNumber, Price = room.Price, UserId = data.UserId, PhoneId = data.PhoneId  };

                    HttpResponseMessage response = await client.PostAsJsonAsync("/api/transaction/payment", tokenData);
                    if (response.IsSuccessStatusCode)
                    {
                        PaymentCardData payment = await response.Content.ReadAsAsync<PaymentCardData>();
                        if (payment.Error == null)
                        {
                            //Ieškomas vartotojas viešbučio duomenų bazėje
                            var user = cnt.User.FirstOrDefault(t => (t.LastName == data.LastName && t.FirstName == data.FirstName));
                            if (user != null)
                            {

                            }
                            else
                            {   //Jeigu nerandamas sukuriamas naujas vartotojas ir įdedamas į duomenų bazę
                                user = new User() { FirstName = data.FirstName, LastName = data.LastName, Address = data.Address, Email = "TEST@TEST.lt", PhoneNumber = data.PhoneNumber };
                                cnt.User.Add(user);
                            }
                            await cnt.SaveChangesAsync();

                            //Vartotojui sukurimas rezervacija
                            var booking = new Booking()
                            {
                                UserId = user.Id,
                                HotelId = 1,
                                Payment = room.Price,
                                Token = data.Token
                            };
                            cnt.Booking.Add(booking);
                            await cnt.SaveChangesAsync();

                            //Testas į rezervaciją pridedamas tik 1 kamabarį, kurį pasirenkame iš POS programos
                            var bookingRoom = new BookingRoom()
                            {
                                BookingId = booking.Id,
                                RoomId = data.Room,
                                DateFrom = DateTime.Parse(data.DateFrom),
                                DateTo = DateTime.Parse(data.DateTo),
                            };
                            cnt.BookingRoom.Add(bookingRoom);

                            await cnt.SaveChangesAsync();

                            //Generuojamas žetonas pagal viešbučio raktui
                            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                            var stringChars = new char[10];
                            var random = new Random();

                            for (int i = 0; i < stringChars.Length; i++)
                            {
                                stringChars[i] = chars[random.Next(chars.Length)];
                            }
                            var tokenNumber = new String(stringChars);

                            return Ok(new PaymentCardData() { Accept = payment.Accept, Error = null, Token = tokenNumber });
                          
                        }
                        else
                        {
                            return Ok(new PaymentCardData() { Accept = null, Error = payment.Error });
                        }
                    }
                    else
                    {
                        return Ok(new PaymentCardData() { Accept = null, Error = "Error access tokenization service" });
                    }
                }
                else
                {
                    return Ok(new PaymentCardData() { Accept = null, Error = "No room"});
                }
            }
        }

    }
}

