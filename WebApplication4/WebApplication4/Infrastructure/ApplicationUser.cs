﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace WebApplication4.Infrastructure
{
    //Update User Columns
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [MaxLength(100)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(100)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(100)]
        public string PhoneID { get; set; }

        [Required]
        public byte Level { get; set; }

        [Required]
        public DateTime JoinDate { get; set; }

        public virtual ICollection<PaymentCard> PaymentCard { get; set; }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            return userIdentity;
        }

    }
  
    public class PaymentCard
    {
        [Key]  
        public int PaymentCardId { get; set; }
        public string CardNumber { get; set; }
        [ForeignKey("ApplicationUser")]
        public string ApplicationUser_Id { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual ICollection<Token> Token { get; set; }
    }

   
    public class Token
    {
        [Key]
        public int TokenId { get; set; }
        public string TokenNumber { get; set; }
        [ForeignKey("TokenType")]
        public int TokenTypeId { get; set; }
        [ForeignKey("PaymentCard")]
        public int PaymentCardId { get; set; }

        public virtual ICollection<TokenInfo> TokenInfo { get; set; }
        public virtual TokenType TokenType { get; set; }
        public virtual PaymentCard PaymentCard { get; set; }
    }
 
    public class TokenInfo
    {
        [Key]
        public int TokenInfoId { get; set; }
        public DateTime? DateUsed { get; set; }
        public string Place { get; set; }
        [ForeignKey("Token")]
        public int TokenId { get; set; }

        public virtual Token Token { get; set; }
    }

    public class TokenType
    {
        [Key]
        public int TokenTypeId { get; set; }
        [Required]
        public string Name { get; set; }

        public virtual ICollection<Token> Token { get; set; }
    }
}