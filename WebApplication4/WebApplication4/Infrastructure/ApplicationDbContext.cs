﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace WebApplication4.Infrastructure
{

 
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("TokenDbContext")
        {
            Configuration.ProxyCreationEnabled = false; //Disable proxy
            Configuration.LazyLoadingEnabled = true; //Disable lazy loading
        }

        public virtual DbSet<Token> Token { get; set; }
        public virtual DbSet<TokenType> TokenType { get; set; }
        public virtual DbSet<TokenInfo> TokenInfo { get; set; }
        public virtual DbSet<PaymentCard> PaymentCard { get; set; }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //Change database tables names
            modelBuilder.Entity<ApplicationUser>().ToTable("User");
            modelBuilder.Entity<IdentityRole>().ToTable("Role");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRole");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaim");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogin");
            modelBuilder.Entity<TokenInfo>().ToTable("TokenInfo");
            modelBuilder.Entity<TokenType>().ToTable("TokenType");
            modelBuilder.Entity<PaymentCard>().ToTable("PaymentCard");
            modelBuilder.Entity<Token>().ToTable("Token");
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

    }
}