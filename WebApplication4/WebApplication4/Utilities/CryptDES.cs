﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace WebApplication4.Utilities
{
    public class CryptDES
    {


        private TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider(String secretKey, String initVector)
        {
            //var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            //Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            //var IVBytes = new byte[16];
            var IVBytes = Encoding.UTF8.GetBytes(initVector);
            //Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new TripleDESCryptoServiceProvider
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128,
                Key = secretKeyBytes,
                IV = IVBytes
            };
        }

        private byte[] Encrypt(byte[] plainBytes, TripleDESCryptoServiceProvider rijndaelManaged)
        {
            return rijndaelManaged.CreateEncryptor()
                .TransformFinalBlock(plainBytes, 0, plainBytes.Length);
        }

        private byte[] Decrypt(byte[] encryptedData, TripleDESCryptoServiceProvider rijndaelManaged)
        {
            return rijndaelManaged.CreateDecryptor()
                .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
        }

        public String Encrypt(String plainText, String key, String initVector)
        {
            var plainBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(Encrypt(plainBytes, tripleDESCryptoServiceProvider(key, initVector)));
        }


        public String Decrypt(String encryptedText, String key, String initVector)
        {
            var encryptedBytes = Convert.FromBase64String(encryptedText);
            return Encoding.UTF8.GetString(Decrypt(encryptedBytes, tripleDESCryptoServiceProvider(key, initVector)));
        }

    }
}