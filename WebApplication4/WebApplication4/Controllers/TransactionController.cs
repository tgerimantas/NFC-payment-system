﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using WebApplication4.Infrastructure;
using WebApplication4.Utilities;

namespace WebApplication4.Controllers
{

    public class TokenData
    {
        public string Token { get; set; }
        public string HotelPaymentNumber { get; set; }
        public float Price { get; set; }
        public string PhoneId { get; set; }
        public string UserId { get; set; }
    }

    public class PaymentData
    {
        public string RecipientCardNumber { get; set; }
        public string SenderCardNumber { get; set; }
        public float Payment { get; set; }
    }

    public class PaymentStatus
    {
        public string Error { get; set; }
        public string Accept { get; set; }
    }

    //Užklausa, kurią vykdo viešbučio sistema persiųsti vartojo duomenis  ir atkoduoti žetoną
    [RoutePrefix("api/transaction")]
    public class TransactionController : ApiController
    {
        private string initVector = "RandomInitVector"; //128bit
        private string key = "m65hhhgfda784d1a"; //128bit

        [Route("payment")]
        [HttpPost]
        public async Task<IHttpActionResult> Transaction(TokenData data)
        {
            var cnt = new ApplicationDbContext();
            if (data.Token != null && data.HotelPaymentNumber != null)
            {
                    
                    using (var client = new HttpClient())
                    {
                        //Iššifruoti žetoną
                        var cryptAes = new CryptAES();
                        var decryptedData = cryptAes.Decrypt(data.Token, key, initVector);
                        if (decryptedData != null)
                        {
                            var token = cnt.Token.FirstOrDefault(t => t.TokenNumber == decryptedData);
                            if (token != null)
                            {
                                var paymentCardId = token.PaymentCardId;
                                var paymentCard = cnt.PaymentCard.FirstOrDefault(t => t.PaymentCardId == paymentCardId);
                                if (paymentCard != null)
                                {
                                    var senderCardNumber = paymentCard.CardNumber;
                                    client.BaseAddress = new Uri("http://localhost:60485/");
                                    client.DefaultRequestHeaders.Accept.Add(
                                        new MediaTypeWithQualityHeaderValue("application/json"));

                                    var cardData = new PaymentData()
                                    {
                                        RecipientCardNumber = data.HotelPaymentNumber,
                                        SenderCardNumber = senderCardNumber,
                                        Payment = data.Price
                                    };

                                    var response =await client.PostAsJsonAsync("/api/transaction/bank", cardData);
                                    if (response.IsSuccessStatusCode)
                                    {
                                        var payment = await response.Content.ReadAsAsync<PaymentStatus>();
                                        return Ok(payment.Error == null ? new PaymentStatus() {Accept = payment.Accept, Error = null} : new PaymentStatus() {Accept = null, Error = payment.Error});
                                    }
                                    else
                                    {
                                        return Ok(new PaymentStatus() {Accept = null, Error = "Bank connection failed"});
                                    }
                                }
                                else
                                {
                                return Ok(new PaymentStatus() { Accept = null, Error = "Service error" }); //No payment Card
                            }
                            }
                            else
                            {
                                return Ok(new PaymentStatus() { Accept = null, Error = "Service error" }); //No token
                            }
                        }
                        else
                        {
                            return Ok(new PaymentStatus() { Accept = null, Error = "Service error" }); //Decryption error
                        }
                    }
                }
                else
                {
                     return Ok(new PaymentStatus() { Accept = null, Error = "Transaction failded" });
                }
            }
           
        }
    }