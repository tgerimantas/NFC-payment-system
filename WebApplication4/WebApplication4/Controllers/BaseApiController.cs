﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using WebApplication4.Infrastructure;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    public class BaseApiController : ApiController
    {

       // private ModelFactory _modelFactory;
        private ApplicationUserManager _AppUserManager = null;
        private ApplicationRoleManager _AppRoleManager = null;

        protected ApplicationRoleManager AppRoleManager
        {
            get
            {
                return _AppRoleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
        }

        protected ApplicationUserManager AppUserManager
        {
            get
            {
                return _AppUserManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        public BaseApiController()
        {
        }

       

        protected HttpResponseMessage GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                 
                    //return BadRequest();
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
                }
                var errors = ModelState.Values.SelectMany(t => t.Errors);
                return Request.CreateResponse(HttpStatusCode.BadRequest, errors);
                //return BadRequest(ModelState);
            }

            return null;
        }
    }
}


//protected ModelFactory TheModelFactory
//{
//    get
//    {
//        if (_modelFactory == null)
//        {
//            _modelFactory = new ModelFactory(this.Request, this.AppUserManager);
//        }
//        return _modelFactory;
//    }
//}