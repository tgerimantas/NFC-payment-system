﻿using System.Linq;
using System.Web.Http;

namespace WebApplication4.Controllers
{
    [RoutePrefix("api/token")]
    public class TokenController : BaseApiController
    {

        [Authorize(Roles = "Admin")]
        [Route("users")]
        [HttpGet]
        public IHttpActionResult GetUsers()
        {
            return Ok(AppUserManager.Users.ToList());
        }
    }
}
