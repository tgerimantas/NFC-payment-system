﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace WebApplication4.Controllers
{
    //Administratoriaus įvairios užklausos
    [RoutePrefix("api/accounts")]
    public class AccountsController : BaseApiController
    {
        [Authorize(Roles = "Admin")]
        [Route("users")]
        [HttpGet]
        public IHttpActionResult GetUsers() //Gaunami visi vartotojai prisiregistravę prie sistemos
        {
            return Ok(AppUserManager.Users.ToList());
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("user/{id:guid}", Name = "GetUserById")] //Pasirenkamas vienas vartotojas
        public async Task<IHttpActionResult> GetUser(string id)
        {
            var user = await AppUserManager.FindByIdAsync(id);

            if (user != null)
            {
                return Ok(user);
            }

            return NotFound();

        }
        [Authorize(Roles = "Admin")]
        [Route("user/{username}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUserByName(string username) //Gaunamas vartotojas pagal vardą
        {
            var user = await AppUserManager.FindByNameAsync(username);

            if (user != null)
            {
                return Ok(user);
            }

            return NotFound();
        }     
    }
}
