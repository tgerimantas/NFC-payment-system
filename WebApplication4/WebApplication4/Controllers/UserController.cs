﻿using Microsoft.AspNet.Identity;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using WebApplication4.Infrastructure;
using WebApplication4.Models;
using WebApplication4.Utilities;

namespace WebApplication4.Controllers
{
    public class CardData
    {
        public string CardNumber { get; set; }
    }

    public class UpdateCardData
    {
        public string Cardid { get; set; }
    }

    public class CardDataBank
    {
        public string CardNumber { get; set; }
    }

    public class RemoveCard
    {
        public string TokenId { get; set; }
    }

    public class CardStatus
    {
        public DateTime Expired { get; set; }
        public string Bank { get; set; }
        public string Error { get; set; }
        public string Accept { get; set; }
    }

    public class PaymentCardData
    {
        public string Error { get; set; }
        public string Accept { get; set; }
        public string Token { get; set; }
        public int Id { get; set; }
        public string Bank { get; set; }
        public string Userid { get; set; }
        public DateTime Expired { get; set; }
    }

    public class UpdateToken
    {
        public string Token { get; set; }
        public string Error { get; set; }
    }

    [RoutePrefix("api/account")]
    public class UserController : BaseApiController
    {
        private string initVector = "RandomInitVector"; //128bit
        private string key = "m65hhhgfda784d1a"; //128bit

        //Sukuriamas vartotojo paskyra
        [AllowAnonymous]
        [Route("create")]
        [HttpPost]
        public async Task<HttpResponseMessage> CreateUser([FromBody] CreateUserBindingModel createUserModel)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(t => t.Errors);
                return Request.CreateResponse(HttpStatusCode.BadRequest, errors);
            }

            var user = new ApplicationUser()
            {
                UserName = createUserModel.Username,
                Email = createUserModel.Email,
                FirstName = createUserModel.FirstName,
                LastName = createUserModel.LastName,
                JoinDate = DateTime.Now.Date,   
                PhoneID = createUserModel.PhoneID          
            };

            IdentityResult addUserResult = await AppUserManager.CreateAsync(user, createUserModel.Password);

            if (!addUserResult.Succeeded)
            {
                return GetErrorResult(addUserResult);
            }

            string[] roles = { "User" };

            IdentityResult addResult = await AppUserManager.AddToRolesAsync(user.Id, roles);

            if (!addResult.Succeeded)
            {
                ModelState.AddModelError("", "Failed to add user roles");
                var errors = ModelState.Values.SelectMany(t => t.Errors);
                return Request.CreateResponse(HttpStatusCode.BadRequest, errors);
            }

        
            return Request.CreateResponse(HttpStatusCode.OK, "CREATED");
        }

        //Gaunama vartotojo informacija
        [Authorize]
        [Route("user")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUserInfo()
        {
            var user = await AppUserManager.FindByIdAsync(User.Identity.GetUserId());
            var crypt = new CryptAES();
            user.Id = crypt.Encrypt(user.Id, key, initVector);
            return Ok(user);
        }

        [Authorize(Roles = "User")]
        [Route("updatecard")]
        [HttpPost]
        public async Task<IHttpActionResult> UpdateToken([FromBody] UpdateCardData data)
        {

            using (var cnt = new ApplicationDbContext())
            {
                //var user = await AppUserManager.FindByIdAsync(User.Identity.GetUserId());
                int ad;
                int.TryParse(data.Cardid, out ad);

                var token =  await cnt.Token.FirstOrDefaultAsync(t => t.TokenId == ad);
                var paymentCard = await cnt.PaymentCard.FirstOrDefaultAsync(t => t.PaymentCardId == token.PaymentCardId);

                //Generuojamas žetonas pagal mokėjimo kortelės numerio ilgį
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                var stringChars = new char[paymentCard.CardNumber.Length];
                var random = new Random();

                for (int i = 0; i < stringChars.Length; i++)
                {
                    stringChars[i] = chars[random.Next(chars.Length)];
                }
                var tokenNumber = new String(stringChars);

                token.TokenNumber = tokenNumber;
                cnt.SaveChanges();

                var crypt = new CryptAES();
                Console.WriteLine("AES -> ENCRYPTION");
                var encryptedToken = crypt.Encrypt(tokenNumber, key, initVector);

                return Ok(new PaymentCardData() { Error = null, Token = encryptedToken });
            }
        }


        //Mokėjimo kortelės registavimas
        [Authorize (Roles = "User")]
        [Route("addcard")]
        [HttpPost]
        public async Task<IHttpActionResult> CreateToken([FromBody] CardData data)
        {

            if (data.CardNumber != null)
            { 
                var user = await AppUserManager.FindByIdAsync(User.Identity.GetUserId());
                bool cards = AppUserManager.Users.Any(t => t.PaymentCard.Any(a => a.CardNumber.Contains(data.CardNumber)));

                if (!cards)
                {
                  
                    using (var client = new HttpClient()) //Kviečia užklausą bankui ir patikrina ar įvesta kortelė egzistuoja
                    {
                        client.BaseAddress = new Uri("http://localhost:60485/");
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        var cardData = new CardDataBank() { CardNumber = data.CardNumber };

                        HttpResponseMessage response = await client.PostAsJsonAsync("/api/card/check", cardData);
                        if (response.IsSuccessStatusCode)
                        {
                            CardStatus card = await response.Content.ReadAsAsync<CardStatus>();
                            if (card.Error == null)
                            {
                                using (var cnt = new ApplicationDbContext())
                                {

                                    //Generuojamas žetonas pagal mokėjimo kortelės numerio ilgį
                                    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                                    var stringChars = new char[data.CardNumber.Length];
                                    var random = new Random();

                                    for (int i = 0; i < stringChars.Length; i++)
                                    {
                                        stringChars[i] = chars[random.Next(chars.Length)];
                                    }
                                    var tokenNumber = new String(stringChars);

                                    var paymentCard = new PaymentCard()
                                    {
                                        ApplicationUser_Id = User.Identity.GetUserId(),
                                        CardNumber = data.CardNumber,
                                    };

                                    cnt.PaymentCard.Add(paymentCard);

                                    var token = new Token()
                                    {
                                        PaymentCardId = paymentCard.PaymentCardId,
                                        TokenNumber = tokenNumber,
                                        TokenTypeId = 2
                                    };

                                    cnt.Token.Add(token);

                                    await cnt.SaveChangesAsync();

                                    //Vykdomas užkodavimas
                                    var crypt = new CryptAES();
                                    var encryptedToken = crypt.Encrypt(tokenNumber, key, initVector);
                                    var userId = crypt.Encrypt(user.Id, key, initVector);

                                    //Siučiamas vartotojui į mobilųjį įrenginį
                                    return Ok(new PaymentCardData() { Token = encryptedToken, Bank = card.Bank, Id = token.TokenId, Error = null, Expired = card.Expired, Accept = card.Accept, Userid = userId });
                                }
                            }
                            else
                            {
                                return Ok(new PaymentCardData() { Error = card.Error });
                            }
                        }
                        else
                        {
                            return Ok(new PaymentCardData() { Error = "Bad bank request" });
                        }
                    }
                }
                else
                {
                    return Ok(new PaymentCardData() { Error = "Selected card is exist in database" });
                }
            }
            else
            {
                return Ok(new PaymentCardData() { Error = "No payment card" });
                
            }
          
        }

        //Mokėjimo kortelės pašalinimas iš vartotojo paskyros kortelių sąrašo
        [Route("removecard")]
        [Authorize]
        [HttpPost]
        public async Task<IHttpActionResult> RemoveCard([FromBody] RemoveCard removeCard)
        {
            using (var cnt = new ApplicationDbContext())
            {
                if (removeCard.TokenId != null)
                {
                    //var user = await this.AppUserManager.FindByIdAsync(User.Identity.GetUserId());
                    var tokenId = int.Parse(removeCard.TokenId);
                    var token = await cnt.Token.FirstOrDefaultAsync(t => t.TokenId == tokenId);
                    var paymentId = token.PaymentCardId;
                    var paymentCard =  await cnt.PaymentCard.FirstOrDefaultAsync(t => t.PaymentCardId == paymentId);
                    try
                    {
                        cnt.PaymentCard.Remove(paymentCard);
                        await cnt.SaveChangesAsync();
                        return Ok(new PaymentCardData() { Accept = "Payment Card removed" });
                    }
                    catch (Exception)
                    {
                        return Ok(new PaymentCardData() { Error = "Payment Card not removed" });
                    }
                }
                else
                {
                    return Ok(new PaymentCardData() { Error = "No payment card" });

                }
            }
            
        }
    }
}
