﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CryptAlgorithmsTesting
{
    class Crypt3DES
    {
        private TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider(string secretKey, string initVector)
        {
            //var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            //Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            //var IVBytes = new byte[16];
            var ivBytes = Encoding.UTF8.GetBytes(initVector);
            //Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new TripleDESCryptoServiceProvider
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                BlockSize = 64,
                Key = secretKeyBytes,
                IV = ivBytes
            };
        }

        private byte[] Encrypt(byte[] plainBytes, TripleDESCryptoServiceProvider element)
        {
            return element.CreateEncryptor()
                .TransformFinalBlock(plainBytes, 0, plainBytes.Length);
        }

        private byte[] Decrypt(byte[] encryptedData, TripleDESCryptoServiceProvider element)
        {
            return element.CreateDecryptor()
                .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
        }

        public string Encrypt(string plainText, string key, string initVector)
        {
            var plainBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(Encrypt(plainBytes, tripleDESCryptoServiceProvider(key, initVector)));
        }


        public string Decrypt(string encryptedText, string key, string initVector)
        {
            var encryptedBytes = Convert.FromBase64String(encryptedText);
            return Encoding.UTF8.GetString(Decrypt(encryptedBytes, tripleDESCryptoServiceProvider(key, initVector)));
        }

    }
}
