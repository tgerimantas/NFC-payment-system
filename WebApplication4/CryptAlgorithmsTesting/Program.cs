﻿using System;
using System.Diagnostics;

namespace CryptAlgorithmsTesting
{
    class Program
    {
        //Naudojamas vykdžiusiam eksperimentui ŠIFRAVIMO ALGORITMO PASIRINKIMO TYRIMAS 4 skyrius
        static void Main()
        {
            Console.WriteLine("TESTING CRYPTROGRAPHY ALGORITHMS");
            Console.WriteLine("SELECT ALGORITHM");
            Console.WriteLine("1.AES");
            Console.WriteLine("2.3DES");
            Console.WriteLine("3.DES");
            Console.WriteLine("0.EXIT");

            var menuSelection = int.Parse( Console.ReadLine());
            var data = "7ad78wad6f7a7d8w1q7a4d7r8a9q7a7a"; //32 bitų tekstas
            var initVector = "RandomInitVector";
            while (menuSelection != 0)
            {
                switch (menuSelection)
                {

                    case 0:
                        Environment.Exit(-1);
                        break;
                    case 1:

                        Console.WriteLine("AES");
                        Console.WriteLine("1. 128bit");
                        Console.WriteLine("2. 192bit");
                        Console.WriteLine("3. 256bit");
                        var keySizeSelection = 0;
                        try
                        {
                            keySizeSelection = int.Parse(Console.ReadLine());
                        }
                        catch (Exception)
                        {
                            Environment.Exit(-1);
                        }
                   
                        switch (keySizeSelection)
                        {
                            case 1:
                                Console.WriteLine("AES");
                                Console.WriteLine("1. 16B"); // 128bit
                                const string key16B = "m65hhhgfda784d1a"; 
                                                               
                                Console.WriteLine("---------------------------------------------------");                              
                                ExCryptAes(data, key16B, initVector);
                                Console.WriteLine("---------------------------------------------------");
                               
                                break;
                            case 2:
                                Console.WriteLine("AES");
                                Console.WriteLine("2. 24B"); // 192bit
                                const string key24B = "m65hhhgfda784d1am65hhhgf";

                                Console.WriteLine("---------------------------------------------------");                                
                                ExCryptAes(data, key24B, initVector);
                                Console.WriteLine("---------------------------------------------------");

                                break;
                            case 3:
                                Console.WriteLine("AES");
                                Console.WriteLine("3. 32B"); //256bit
                                const string key32B = "m65hhhgfda784d1am65hhhgfda784d1a";
                                Console.WriteLine("---------------------------------------------------");
                               
                                ExCryptAes(data, key32B, initVector);
                                Console.WriteLine("---------------------------------------------------");
                                break;
                            default:
                                Environment.Exit(-1);
                                break;
                        }
                                               
                        break;
                    case 2:

                        Console.WriteLine("3DES");
                        Console.WriteLine("1. 112bit");
                        Console.WriteLine("2. 168bit");
                        keySizeSelection = 0;
                        try
                        {
                            keySizeSelection = int.Parse(Console.ReadLine());
                        }
                        catch (Exception)
                        {

                            Environment.Exit(-1);
                        }
                        switch (keySizeSelection)
                        {
                            case 1:
                                Console.WriteLine("3DES");
                                Console.WriteLine("1. 112 bit"); //128bit
                                const string key1 = "m65hhhgfda784d1a"; //3216549832165498

                                Console.WriteLine("---------------------------------------------------");                              
                                ExCrypt3Des(data, key1, "RandomIn");
                                Console.WriteLine("---------------------------------------------------");
                                
                                break;
                            case 2:
                                Console.WriteLine("3DES");
                                Console.WriteLine("2. 168bit"); //192bit
                                const string key2 = "m65hhhgfda784d1am65hhhgf";

                                Console.WriteLine("---------------------------------------------------");                               
                                ExCrypt3Des(data, key2, "RandomIn");
                                Console.WriteLine("---------------------------------------------------");
                                break;
                            default:
                                Environment.Exit(-1);
                                break;
                        }
                        
                        break;
                    case 3:
                        Console.WriteLine("DES");

                        Console.WriteLine("1. 56bit");
                        keySizeSelection = 0;
                        try
                        {
                            keySizeSelection = int.Parse(Console.ReadLine());
                        }
                        catch (Exception)
                        {
                           Environment.Exit(-1);
                        }
                        switch (keySizeSelection)
                        {
                            case 1:
                                Console.WriteLine("DES");
                                Console.WriteLine("1. 56bit"); //64
                                const string key1 = "m65hhhgf";
                               
                                Console.WriteLine("---------------------------------------------------");                              
                                ExCryptDes(data, key1, "RandomIn");
                                Console.WriteLine("---------------------------------------------------");
                                
                                break;
                            default:
                                Environment.Exit(-1);
                                break;

                        }
                        break;
                }
            }
        }


        public static void ExCryptAes(string data, string key, string initVector)
        {
            
            var crypt = new CryptAES();
            Console.WriteLine("AES -> ENCRYPTION");
            var watchEn = Stopwatch.StartNew();            
            var encryptedData = crypt.Encrypt(data, key, initVector);
            watchEn.Stop();
            var elapsedMsEn = watchEn.Elapsed.TotalMilliseconds;           
            Console.WriteLine("Encryption data: " + encryptedData);
            Console.WriteLine("Encryption time: " + elapsedMsEn);          
            Console.WriteLine("AES -> DECRYPTION");         
            var watchDe = Stopwatch.StartNew();
            var decryptedData = crypt.Decrypt(encryptedData, key, initVector);
            watchDe.Stop();
            var elapsedMsDe = watchDe.Elapsed.TotalMilliseconds;            
            Console.WriteLine("Decryption data: " + decryptedData);
            Console.WriteLine("Decryption time: " + elapsedMsDe);
            
        }



        public static void ExCrypt3Des(string data, string key, string initVector)
        {
          
            var crypt = new Crypt3DES();
            Console.WriteLine("3DES -> ENCRYPTION");
            var watchEn = Stopwatch.StartNew();
            var encryptedData = crypt.Encrypt(data, key, initVector);
            watchEn.Stop();
            var elapsedMsEn = watchEn.Elapsed.TotalMilliseconds;
            Console.WriteLine("Encryption data: " + elapsedMsEn);
            Console.WriteLine("3DES -> DECRYPTION");
            var watchDe = Stopwatch.StartNew();
            var decryptedData = crypt.Decrypt(encryptedData, key, initVector);
            watchDe.Stop();
            var elapsedMsDe = watchDe.Elapsed.TotalMilliseconds;
            Console.WriteLine("Decryption data: " + decryptedData);
            Console.WriteLine("Decryption time: " + elapsedMsDe);
        }




        public static void ExCryptDes(string data, string key, string initVector)
        {
            
            var crypt = new CryptDES();
            Console.WriteLine("DES -> ENCRYPTION");            
            var watchEn = Stopwatch.StartNew();
            var encryptedData = crypt.Encrypt(data, key, initVector);
            watchEn.Stop();
            var elapsedMsEn = watchEn.Elapsed.TotalMilliseconds;          
            Console.WriteLine("Encryption data: " + encryptedData);
            Console.WriteLine("Encryption time: " + elapsedMsEn);
            Console.WriteLine("DES -> DECRYPTION");          
            var watchDe = Stopwatch.StartNew();
            var decryptedData = crypt.Decrypt(encryptedData, key, initVector);
            watchDe.Stop();
            var elapsedMsDe = watchDe.Elapsed.TotalMilliseconds;           
            Console.WriteLine("Decryption time: " + elapsedMsDe);
            Console.WriteLine("Decryption data: " + decryptedData);
        }
    }
}
