﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace CryptAlgorithmsTesting
{
    class CryptDES
    {
        private DESCryptoServiceProvider DESCryptoServiceProvider(String secretKey, String initVector)
        {
            //var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            //Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            //var IVBytes = new byte[16];
            var ivBytes = Encoding.UTF8.GetBytes(initVector);
            //Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new DESCryptoServiceProvider
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                BlockSize = 64,
                Key = secretKeyBytes,
                IV = ivBytes
            };
        }

        private byte[] Encrypt(byte[] plainBytes, DESCryptoServiceProvider rijndaelManaged)
        {
            return rijndaelManaged.CreateEncryptor()
                .TransformFinalBlock(plainBytes, 0, plainBytes.Length);
        }

        private static byte[] Decrypt(byte[] encryptedData, DESCryptoServiceProvider rijndaelManaged)
        {
            return rijndaelManaged.CreateDecryptor()
                .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
        }

        public string Encrypt(String plainText, String key, String initVector)
        {
            var plainBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(Encrypt(plainBytes, DESCryptoServiceProvider(key, initVector)));
        }


        public string Decrypt(String encryptedText, String key, String initVector)
        {
            var encryptedBytes = Convert.FromBase64String(encryptedText);
            return Encoding.UTF8.GetString(Decrypt(encryptedBytes, DESCryptoServiceProvider(key, initVector)));
        }

    }
}

