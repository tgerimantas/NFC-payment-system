﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace CryptAlgorithmsTesting
{
    class CryptAES
    {

        private AesCryptoServiceProvider GetRijndaelManaged(string secretKey, string initVector)
        {
            //var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            //Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            //var IVBytes = new byte[16];
            var ivBytes = Encoding.UTF8.GetBytes(initVector);
            //Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));
            return new AesCryptoServiceProvider
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                BlockSize = 128,
                Key = secretKeyBytes,
                IV = ivBytes
            };
        }

        private byte[] Encrypt(byte[] plainBytes, AesCryptoServiceProvider rijndaelManaged)
        {
            return rijndaelManaged.CreateEncryptor()
                .TransformFinalBlock(plainBytes, 0, plainBytes.Length);
        }

        private byte[] Decrypt(byte[] encryptedData, AesCryptoServiceProvider rijndaelManaged)
        {
            return rijndaelManaged.CreateDecryptor()
                .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
        }

        public string Encrypt(string plainText, String key, string initVector)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            var plainBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(Encrypt(plainBytes, GetRijndaelManaged(key, initVector)));
        }


        public String Decrypt(String encryptedText, String key, String initVector)
        {
            var encryptedBytes = Convert.FromBase64String(encryptedText);
            return Encoding.UTF8.GetString(Decrypt(encryptedBytes, GetRijndaelManaged(key, initVector)));
        }
    }
}
